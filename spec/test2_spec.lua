
do return end
local test = require("u-test")

function rungame()
	f = io.popen("bin\\client\\bin\\brokengine_client.exe spec/headless","r")
	d = f:read("*all")
	f:close()
	return d
end

function runboth()
	f1 = io.popen("bin\\server\\bin\\brokengine_server.exe spec/server","r")
	f2 = io.popen("bin\\client\\bin\\brokengine_client.exe spec/headless","r")
	d1 = f1:read("*all")
	d2 = f2:read("*all")
	f1:close()
	f2:close()
	return d1, d2
end

function writegame(...)
	f = io.open("spec/headless/init.lua","w")
	data = {"GAME.crashy()"}
	for _,v in pairs({...}) do
		data[#data + 1] = v
	end
	data[#data + 1] = "\nGAME.exit()\n"
	f:write(table.concat(data,"\n"))
	f:close()
end

function writeserver(...)
	f = io.open("spec/server/init.lua","w")
	data = {...}
	--data[#data + 1] = "\nGAME.exit()\n"
	f:write(table.concat(data,"\n"))
	f:close()
end

function assert_game_runs()
	test.is_not_nil(rungame():find("\nGoodbye\n$"))
end
function assert_both_run()
	local a,b = runboth()
	test.is_not_nil(a:find("\nGoodbye\n$"))
	test.is_not_nil(b:find("\nGoodbye\n$"))
end

test.game_runs = function()
	writegame("")
	assert_game_runs()
end

test.gui.elements_exist = function()
	for k,v in pairs({
		buttons = "newbutton",
		checkboxes = "newcheckbox",
		colorselectors = "newcolorselector",
		editboxes = "neweditbox",
		openfiledialogs = "newfileopendialog",
		images = "newiguiimage",
		labels = "newlabel",
		spinboxes = "newspinbox",
		treeviews = "newtreeview",
		windows = "newwindow",
	}) do
		writegame("assert(gui." .. v .. ")")
		assert_game_runs()
	end
end

test.gui.screen_functions = function()
	writegame("assert(scrw)")
	assert_game_runs()
	writegame("assert(scrh)")
	assert_game_runs()
end

--#define PAIR 1
--#define BUS 2
--#define PUB 3
--#define SUB 4
--#define PULL 5
--#define PUSH 6
--#define REQ 7
--#define REP 8
--#define RESPOND 9
--#define SURVEY 10
local protocols = {
	"PAIR", "BUS", "PUB", "SUB", "PULL", "PUSH", "REQ", "REP", "RESPOND", "SURVEY"
}
test.net.protocols_exist = function()
	for k,v in pairs(protocols) do
		writegame(string.format("assert(net.%s)",v))
		assert_game_runs()
	end
end

for _,sockettype in pairs(protocols) do
	for _,func in pairs({
		"bind","connect","send"
	}) do
		test.net[sockettype .. "_socket_has_" .. func .. "_function"] = function()
			writegame(string.format([[
				local socket = net.newsocket(net.%s)
				assert(socket.%s)
				assert(type(socket.%s) == "function")
			]],sockettype,func,func))
			assert_game_runs()
		end
	end
end
--test.net.functions_exist = function()
	--writegame("assert(net.newsocket and type(net.newsocket == 'function'))")
	--assert_game_runs()
	--for _,sockettype in pairs(protocols) do
		--for _,func in pairs({
		--}) do
			--writegame(string.format([[
				--local socket = net.newsocket(net.%s)
				--assert(socket.%s)
				--assert(type(socket.%s) == "function")
			--]],sockettype,func,func))
			--assert_game_runs()
		--end
	--end
--end

test.net.protocol_pair = function()
	writeserver([[
		local has_ponged = false
		local socket  = net.newsocket(net.PAIR)
		socket:bind("tcp://127.0.0.1:5555")
		function socket:receive(stream)
			print("Socket receive triggered")
			has_ponged = true
			local message = stream:readstring()
			assert(message == "ping")
			socket:send(function(stream2)
				stream2:writestring("pong")
			end)
		end
		function GAME.tick()
			if has_ponged then
				GAME.exit()
			end
		end
	]])
	writegame([[
		local socket = net.newsocket(net.PAIR)
		socket:connect("tcp://127.0.0.1:5555")
		function socket:receive(stream)
			local message = stream:readstring()
			assert(message == "pong")
		end
		socket:send(function(stream)
			stream:writestring("ping")
		end)
	]])
	assert_both_run()
end

--test.net.protocol_pubsub = function()
	--writeserver([[
		--local counter = 0
		--local max_count = 10000 --How long to live for,
					----since we can't detect when the user
					----sends the message
		--local socket = net.newsocket(net.PUB)
		--socket:bind("tcp://127.0.0.1:5555")
		--function GAME.tick()
			--if counter < max_count then
				--counter = counter + 1
				--socket:send(function(stream)
					--stream:writestring("ping")
				--end)
			--else
				--GAME.exit()
			--end
		--end
	--]])
	--writegame([[
		--local socket = net.newsocket(net.SUB)
		--socket:connect("tcp://127.0.0.1:5555")
		--local oldgameexit = GAME.exit
		--socket:receive(function(stream)
			--assert(stream:readstring() == "ping")
			--oldgameexit()
		--end)
		--function GAME.exit() return end --Stop the client from exiting
	--]])
--end

test.summary()
