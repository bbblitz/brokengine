
local game_bin = nil
if package.config:sub(1,1) == "/" then -- linux or osx
	game_bin = "bin/client/bin/brokengine_client"
	server_bin = "bin/server/bin/brokengine_server"
else
	game_bin = "bin\\client\\bin\\brokengine_client.exe"
	server_bin = "bin\\server\\bin\\brokengine_server.exe"
end

function rungame()
	f = io.popen(game_bin .. " spec/headless","r")
	d = f:read("*all")
	f:close()
	return d
end

function runboth()
	--print("Running both")
	--Do we have a race condition here? (Can client start and send it's message
	--before the server is ready to accept?
	f1 = io.popen(server_bin .. " spec/server","r")
	f2 = io.popen(game_bin .. " spec/headless","r")
	--print("Both ran...")
	d1 = f1:read("*all")
	d2 = f2:read("*all")
	--print("Both read all")
	f1:close()
	f2:close()
	--print("returning")
	return d1, d2
end

function writegame(...)
	f = io.open("spec/headless/init.lua","w")
	data = {"GAME.crashy()"}
	for _,v in pairs({...}) do
		data[#data + 1] = v
	end
	data[#data + 1] = "\nGAME.exit()\n"
	f:write(table.concat(data,"\n"))
	f:close()
end

function writeserver(...)
	f = io.open("spec/server/init.lua","w")
	data = {...}
	data[#data + 1] = "\nGAME.exit()\n"
	f:write(table.concat(data,"\n"))
	f:close()
end

function assert_game_runs()
	test.is_not_nil(rungame():find("\nGoodbye\n$"))
end
function assert_both_run()
	local a,b = runboth()
	assert.is_not_nil(a:find("\nGoodbye\n$"))
	assert.is_not_nil(b:find("\nGoodbye\n$"))
end
describe("networking",function()
	it("should communicate with PAIR sockets",function()
		writeserver([[
			local has_ponged = false
			local socket  = net.newsocket(net.PAIR)
			socket:bind("tcp://127.0.0.1:5555")
			function socket:receive(stream)
				print("Socket receive triggered")
				has_ponged = true
				local message = stream:readstring()
				assert(message == "ping")
				socket:send(function(stream2)
					stream2:writestring("pong")
				end)
			end
			function GAME.tick()
				if has_ponged then
					GAME.exit()
				end
			end
		]])
		--print("writing game...")
		writegame([[
			local socket = net.newsocket(net.PAIR)
			socket:connect("tcp://127.0.0.1:5555")
			function socket:receive(stream)
				local message = stream:readstring()
				assert(message == "pong")
			end
			socket:send(function(stream)
				stream:writestring("ping")
			end)
		]])
		--print("asserting both run...")
		assert_both_run()
	end)
end)
