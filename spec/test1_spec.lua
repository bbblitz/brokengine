print("Hello, world!")

--[[Create the headless client init file]]
local f = io.open("spec/headless/deviceinit.lua","w")
f:write([=[
return {
    ["Anti Alias"] = 16,
    ["Bits Per Pixel"] = 16,
    ["Device Type"] = "BEST",
    ["Display Adapter"] = 0,
    ["Double Buffer"] = true,
    ["Multithreaded"] = false,
    ["Driver Type"] = "NULL",
    ["Fullscreen"] = false,
    ["Stencil Buffer"] = true,
    ["Stereo Buffer"] = false,
    ["VSync"] = true,
    ["Window Width"] = 640,
    ["Window Height"] = 480,
}
]=])
f:close()

local game_bin = nil
if package.config:sub(1,1) == "/" then -- linux or osx
	game_bin = "bin/client/bin/brokengine_client"
else
	game_bin = "bin\\client\\bin\\brokengine_client.exe"
end

function rungame()
	f = io.popen(game_bin .. " spec/headless","r")
	d = f:read("*all")
	f:close()
	return d
end

function writegame(...)
	f = assert(io.open("spec/headless/init.lua","w"))
	data = {"GAME.crashy()"}
	for _,v in pairs({...}) do
		data[#data + 1] = v
	end
	data[#data + 1] = "\nGAME.exit()\n"
	f:write(table.concat(data))
	f:close()
end

function assert_game_runs()
	assert.truthy(rungame():find("\nGoodbye\n$"))
end

describe("Brok[en]gine",function()
	it("should run",function()
		writegame()
		d = rungame()
		assert_game_runs()
	end)
	it("should provide a lua environment",function()
		writegame("print(\"Hello from lua!\")")
		d = rungame()
		assert.truthy(d:find("\nHello from lua!\n"))
	end)

	for k,v in pairs({
		buttons = "newbutton",
		checkboxes = "newcheckbox",
		colorselectors = "newcolorselector",
		editboxes = "neweditbox",
		openfiledialogs = "newfileopendialog",
		images = "newiguiimage",
		labels = "newlabel",
		spinboxes = "newspinbox",
		treeviews = "newtreeview",
		windows = "newwindow",
	}) do
		it("should provide functions to make gui " .. k,function()
			writegame("assert(gui." .. v .. ")")
			assert_game_runs()
		end)
	end

	it("should provide functions to get the width and height of the screen",function()
		writegame("assert(scrw)")
		assert_game_runs()
		writegame("assert(scrh)")
		assert_game_runs()
	end)
end)
