# Brok[en]gine

![logo](https://cogarr.net/files/alex/brokenengine_small.png)

Broken Engine is a hobby game engine that glues [Bullet Physics](bulletphysics.org/wordpress), [Irrlicht](irrlicht.sourceforge.net), [Lua](www.lua.org), and [various](nanomsg.org) [other](www.boost.org) technologies togeather to give a realtime, 3d, physics engine.

## Download

Binaries will not be provided until Brok[en]gine reaches version 1.0, if you want to try it out before then, you will have to build the engine yourself. 

Alternatively, you could download either of the [two](https://cogarr.net/source/cgit.cgi/home_text_adventure/) [games](http://cogarr.net/source/cgit.cgi/mahjong_solitaire/about/) to see Brok[en]gine in action!

1. Use [git](git-scm.com) to download this with submodules `git clone --recurse-submodules https://cogarr.net/source/cgi.cgit/brokengine` 
2. Download make, g++, ect. Windows users can use [mingw](https://mingw.org). Open a terminal and cd into this folder, and run `make DEBUG=true`
  1. For some god-awful reason, when useing MSYS2 under windows, you need to define MSYSTEM=MINGW64 as an environment variable to get luajit to compile. `set MSYSTEM=MINGW64`
3. Binaries can be found in bin/(client|server)/bin/
4. Initally ran scripts can be found at bin/(client|server)/data/init.lua

## Documentation
Documentation is built using [ldoc](https://github.com/stevedonovan/LDoc), just use cd into this folder and use `ldoc .`; documentation is located under the `/doc/` folder. Open index.html to view.

## Misc.
This project is under very heavy development. Expect lots of code churn for the foreseeable future.
#### Msys2
Under msys2, I had to install the w32api headers `pacman -Syu msys/msys2-w32api-headers`
Then comment out the closeing curly brace at line 1128 of `/usr/include/w32api/tchar.h`
Then use the mingw64 shell in C:/msys2/mingw64.exe (make sure `gcc -dumpmachine` outputs `x86_64-w64-mingw32` or something and not `x86_64-pc-msys`) to `make DEBUG=true`
Then install the mingw64 version of make, and use it to create the makefile for lib/bullet


### Roadmap
0. <del>Lua-defined textures</del>
0. <del>Lua-defined materials</del>
0. Bind all the irrlicht gui things to lua
0. Lua-defined models
0. Lua-defined hitboxes
0. Networking utilities
0. Make a tech demo game
