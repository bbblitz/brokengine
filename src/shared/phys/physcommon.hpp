#ifndef _shared_physcommon_
#define _shared_physcommon_
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
void gameloop_phys(void(*f)(btCollisionObject *));
void phys_genesis();
void phys_shutdown();
extern btDiscreteDynamicsWorld* World;
#endif
