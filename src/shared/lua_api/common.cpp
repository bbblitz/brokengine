
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

#include <stdlib.h>
#include "common.hpp"

#define set_const(l,x) lua_pushstring(l,#x);lua_pushinteger(l,x);lua_settable(l,-3);

//Expose things to the lua state
void loadLLibs(lua_State* L){

	lua_pushcfunction(L,luaopen_base);
	lua_pushliteral(L,"");
	lua_call(L,1,0);

	lua_pushcfunction(L,luaopen_table);
	lua_pushliteral(L,LUA_TABLIBNAME);
	lua_call(L,1,0);

	lua_pushcfunction(L,luaopen_string);
	lua_pushliteral(L,LUA_STRLIBNAME);
	lua_call(L,1,0);

	lua_pushcfunction(L,luaopen_math);
	lua_pushliteral(L,LUA_MATHLIBNAME);
	lua_call(L,1,0);
	
	lua_pushcfunction(L,luaopen_string);
	lua_pushliteral(L,LUA_STRLIBNAME);
	lua_call(L,1,0);

	/*
	lua_pushcfunction(L,luaopen_string);
	lua_pushliteral(L,LUA_STRLIBNAME);
	lua_call(L,1,0);
	*/
}

int pushvector4i(lua_State* L,long a, long b, long c, long d){
	lua_newtable(L);//{}

	lua_pushinteger(L,1);//{},1
	lua_pushinteger(L,a);//{},1,a
	lua_settable(L,-3);//{}

	lua_pushinteger(L,2);//{},2
	lua_pushinteger(L,b);//{},2,b
	lua_settable(L,-3);//{}

	lua_pushinteger(L,3);//{},3
	lua_pushinteger(L,c);//{},3,c
	lua_settable(L,-3);//{}

	lua_pushinteger(L,4);//{},4
	lua_pushinteger(L,d);//{},4,d
	lua_settable(L,-3);{}

	return 1;
}

int pushvector3i(lua_State* L,long a,long b,long c){
	lua_newtable(L);//{}

	lua_pushinteger(L,1);//{},1
	lua_pushinteger(L,a);//{},1,a
	lua_settable(L,-3);//{}

	lua_pushinteger(L,2);//{},2
	lua_pushinteger(L,b);//{},2,b
	lua_settable(L,-3);//{}

	lua_pushinteger(L,3);//{},3
	lua_pushinteger(L,c);//{},3,c
	lua_settable(L,-3);//{}

	return 1;
}
int pushvector3d(lua_State* L,double a,double b,double c){
	lua_newtable(L);//{}

	lua_pushinteger(L,1);//{},1
	lua_pushnumber(L,a);//{},1,a
	lua_settable(L,-3);//{}

	lua_pushinteger(L,2);//{},2
	lua_pushnumber(L,b);//{},2,b
	lua_settable(L,-3);//{}

	lua_pushinteger(L,3);//{},3
	lua_pushnumber(L,c);//{},3,c
	lua_settable(L,-3);//{}

	return 1;
}
int pushvector2i(lua_State* L, long a, long b){
	lua_newtable(L);
	
	lua_pushinteger(L,1);
	lua_pushinteger(L,a);
	lua_settable(L,-3);
	
	lua_pushinteger(L,2);
	lua_pushinteger(L,b);
	lua_settable(L,-3);

	return 1;
}

int popvector4i(lua_State* L,long* a,long* b,long* c, long* d){
	lua_pushinteger(L,1);//{v4},1
	lua_gettable(L,-2);//{v4},v4[1]
	*a = lua_tointeger(L,-1);//{v4},v4[1]
	lua_pop(L,1);//{v4}

	lua_pushinteger(L,2);//{v4},2
	lua_gettable(L,-2);//{v4},v4[2]
	*b = lua_tointeger(L,-1);//{v4},v4[2]
	lua_pop(L,1);//{v4}

	lua_pushinteger(L,3);//{v4},3
	lua_gettable(L,-2);//{v4},v4[3]
	*c = lua_tointeger(L,-1);//{v4},v4[3]
	lua_pop(L,1);//{v4}

	lua_pushinteger(L,4);//{v4},3
	lua_gettable(L,-2);//{v4},v4[3]
	*d = lua_tointeger(L,-1);//{v4},v4[3]
	lua_pop(L,1);//{v4}

	lua_pop(L,1);//
	return 0;
}

int popvector3i(lua_State* L,long* a,long* b,long* c){//{v3}
	lua_pushinteger(L,1);//{v3},1
	lua_gettable(L,-2);//{v3},v3[1]
	*a = lua_tointeger(L,-1);//{v3},v3[1]
	lua_pop(L,1);//{v3}

	lua_pushinteger(L,2);//{v3},2
	lua_gettable(L,-2);//{v3},v3[2]
	*b = lua_tointeger(L,-1);//{v3},v3[2]
	lua_pop(L,1);//{v3}

	lua_pushinteger(L,3);//{v3},3
	lua_gettable(L,-2);//{v3},v3[3]
	*c = lua_tointeger(L,-1);//{v3},v3[3]
	lua_pop(L,1);//{v3}

	lua_pop(L,1);//
	return 0;
}



int popvector3d(lua_State* L,double* a,double* b,double* c){
	lua_pushinteger(L,1);
	lua_gettable(L,-2);
	*a = lua_tonumber(L,-1);
	lua_pop(L,1);

	lua_pushinteger(L,2);
	lua_gettable(L,-2);
	*b = lua_tonumber(L,-1);
	lua_pop(L,1);

	lua_pushinteger(L,3);
	lua_gettable(L,-2);
	*c = lua_tonumber(L,-1);
	lua_pop(L,1);

	lua_pop(L,1);
	return 0;
}

int popvector4d(lua_State* L, double *a, double *b, double *c, double *d){
	lua_pushinteger(L,1);//{a,b,c,d},1
	lua_gettable(L,-2);//{a,b,c,d},a
	*a = lua_tonumber(L,-1);//{a,b,c,d},a
	lua_pop(L,1);//{a,b,c,d}
	
	lua_pushinteger(L,2);
	lua_gettable(L,-2);
	*b = lua_tonumber(L,-1);
	lua_pop(L,1);
	
	lua_pushinteger(L,3);
	lua_gettable(L,-2);
	*c = lua_tonumber(L,-1);
	lua_pop(L,1);
	
	lua_pushinteger(L,4);
	lua_gettable(L,-2);
	*d = lua_tonumber(L,-1);
	lua_pop(L,1);

	lua_pop(L,1);
	return 0;
}

//{{sx,sy},{ex,ey}}
int poprecti(lua_State* L, long *sx, long *sy, long *ex, long *ey){
	lua_pushnumber(L,1);
	lua_gettable(L,-2);
	popvector2i(L,sx,sy);
	lua_pushnumber(L,2);
	lua_gettable(L,-2);
	popvector2i(L,ex,ey);
	lua_pop(L,1);
	return 0;
}

int pushrecti(lua_State* L, long sx, long sy, long ex, long ey){
	lua_newtable(L);//{}
	lua_pushnumber(L,1);//{},1,{sx,sy}
	pushvector2i(L,sx,sy);//{},1,{sx,sy}
	lua_settable(L,-3);//{{sx,sy}}
	lua_pushnumber(L,2);//{{sx,sy}},2
	pushvector2i(L,ex,ey);//{{sx,sy}},2,{ex,ey}
	lua_settable(L,-3);//{{sx,sy},{ex,ey}}
	return 1;
}

int popvector2i(lua_State* L, long* a, long* b){
	lua_pushinteger(L,1);
	lua_gettable(L,-2);
	*a = lua_tonumber(L,-1);
	lua_pop(L,1);

	lua_pushinteger(L,2);
	lua_gettable(L,-2);
	*b = lua_tonumber(L,-1);
	lua_pop(L,2);
	
	return 0;
}

//When crashy is enabled, errors that usually get caught by errfunc crash instead
//Useful for testing
bool crashy = false;
//errfunc("mssg",false)
int errfunc(lua_State *L){
	printf("Error function called\n");
	int nargs = lua_gettop(L);
	for(int i = 1; i <= nargs; i++){
		printf("Arguemtn %d to errorfun was type %s\n",i,lua_typename(L,lua_type(L,i)));
	}
	if (!lua_isstring(L, 1)){  /* 'message' not a string? */
		printf("Error is not a string, it is a %s\n",lua_typename(L,lua_type(L,1)));
		//return 1;  [> keep it intact <]
	}
	printf("Error:%s\n",lua_tostring(L,1));
	lua_getglobal(L, "debug");//error,{debug}
	lua_getglobal(L, "print");//error,{debug},print
	if (!lua_istable(L, -2)) {
		printf("debug is not a table, abort\n");
		lua_pop(L, 1);
		return 1;
	}
	lua_getfield(L, -2, "traceback");//error,{debug},print,debug.traceback
	if (!lua_isfunction(L, -1)) {
		printf("debug does not have a .traceback function, abort\n");
		lua_pop(L, 2);
		return 1;
	}
	//lua_pushvalue(L, -4); //error,{debug},print,debug.traceback,error
	//lua_pushinteger(L, 3); //error,{debug},print,debug.traceback,error,2
	lua_call(L, 0, 1);
	//lua_call(L, 2, 1);//error,{debug},print,str_traceback 
	printf("--------------------------\n");
	lua_call(L,1,0);//error,{debug}
	lua_pop(L,1);
	printf("Returning:");
	printf("crashy is %d", crashy);
	if(crashy){
		exit(-1);
	}
	return 1;
}
void pusherrorfunc(lua_State *L){
	lua_pushcfunction(L,errfunc);
}
int make_crashy(lua_State *L){
	crashy = true;
	printf("Setting crashy to %d\n",crashy);
	return 0;
}
