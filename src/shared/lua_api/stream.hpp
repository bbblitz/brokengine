#include <stdlib.h>
#include <nng/nng.h>

//Char is not definitvely a byte, read the fucking standard
#define byte char

typedef struct stream {
	long length;
	byte* data;
	long read;
	nng_pipe *pipe;
} stream;

struct stream* stream_create();

void stream_writeInt(struct stream* s, int number);
int stream_readInt(struct stream* s);

void stream_writeString(struct stream* s, const char* string, size_t len);
char* stream_readString(struct stream* s);

void stream_writeData(struct stream* s, const char* data, size_t len);
void stream_readData(struct stream* s, int len, char* out);

void stream_writeDouble(struct stream* s, double number);
double stream_readDouble(struct stream* s);

void stream_print(struct stream* s);

void stream_free(struct stream* s);
