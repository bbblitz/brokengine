#include <chrono>
#include <shared/lua_api/load_common.hpp>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
using namespace std::chrono;

//Gets the time
int get_time(lua_State* L){
	std::chrono::high_resolution_clock::time_point now = high_resolution_clock::now();
	std::chrono::high_resolution_clock::duration since_epoch = now.time_since_epoch();
	double dc = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch).count();
	lua_pushnumber(L,dc);
	return 1;
}

void loadCommonLibs(lua_State* L){
	lua_getglobal(L,"GAME");
	lua_pushcfunction(L,make_crashy);
	lua_setfield(L,-2,"crashy");
	lua_pop(L,1);
	lua_pushcfunction(L,get_time);
	lua_setglobal(L,"get_time");
}

void gameloop_common(lua_State* L){

}
