#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stream.hpp"

char stx = 0x002;
char etx = 0x003;

struct stream* stream_create(){
	struct stream* s = (struct stream*)malloc(sizeof(struct stream));
	s->length = 0;
	s->data = (byte*)malloc(sizeof(byte)*0);
	s->read = 0;
	s->pipe = NULL;
	return s;
}
void stream_writeInt(struct stream* s, int number){
	long o = s->length;
	s->length += sizeof(int);
	s->data = (byte*)realloc(s->data,s->length);
	int* ptr = (int*)(s->data + o);
	*ptr = number;
}
int stream_readInt(struct stream* s){
	int* ptr = (int*)(s->data + s->read);
	s->read += sizeof(int);
	return *ptr;
}
void stream_writeData(struct stream* s, const char* d, size_t len){
	long o = s->length;
	s->length += sizeof(char)*len;
	s->data = (byte*)realloc(s->data,s->length);
	char* ptr = (char*)(s->data + o);
	memcpy(ptr,d,len);
}
void stream_readData(struct stream* s, int len, char* out){
	char* ptr = (char*)(s->data + s->read);
	memcpy(out,ptr,len);
	s->read += len;
}
void stream_writeString(struct stream* s, const char* str, size_t len){
	stream_writeData(s,str,len);
	stream_writeData(s,"\0",1);//Null character
}
char* stream_readString(struct stream* s){
	char* ptr = (char*)(s->data + s->read);
	s->read += strlen(ptr) + 1;//+1 for null character
	return ptr;
}
void stream_writeDouble(struct stream* s, double number){
	long o = s->length;
	s->length += sizeof(double);
	s->data = (byte*)realloc(s->data,s->length);
	double* ptr = (double*)(s->data + o);
	*ptr = number;
}
double stream_readDouble(struct stream* s){
	double *ptr = (double*)(s->data + s->read);
	s->read += sizeof(double);
	return *ptr;
}

void stream_print(struct stream* s){
	printf("Length:%ld\nRead:%ld\nData starts at %p\nData:",s->length, s->read, s->data);
	long i = 0;
	byte* ptr = s->data;
	while(i < s->length){
		printf("%4X",*(ptr + i));
		i++;
	}
	printf("\n");
}

void stream_free(struct stream* s){
	free(s->data);
	free(s);
}
