extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

void load_physfuncs(lua_State* L);
