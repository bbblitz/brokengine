#ifndef _BTACTION_HPP_
#include <stdio.h>
#include <stdlib.h>
extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}
int baction_register(lua_State* L);
void makeaction(lua_State *L);
#endif
