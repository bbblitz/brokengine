
#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>

void bghostobject_register(lua_State* L);
void makeghostobject(lua_State* L);
