

#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void bphysbox_register(lua_State* L);
void makenewbphysbox(lua_State* L);
