extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}
#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
#include <shared/lua_api/common.hpp>

extern btDiscreteDynamicsWorld* World;

//phys.newboxshape({ax,ay,az})
static int newboxshape(lua_State* L){
	double ax,ay,az;
	popvector3d(L,&ax,&ay,&az);
	ax *= 0.5;
	ay *= 0.5;
	az *= 0.5;
	btBoxShape *bs = new btBoxShape(btVector3(ax,ay,az));
	printf("Created shape: %p\n",(void*)bs);	
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,bs);//{},ud_bs
	lua_setfield(L,-2,"shape");//{}
	luaL_getmetatable(L,"phys.shape");//{},{m_shape}
	lua_setmetatable(L,-2);//{}

	return 1;
}

static const luaL_reg bshape_f[] = {
	{"newboxshape",           newboxshape},
	{0,0},
};

static const luaL_reg bshape_m[] = {
	{0,0},
};

int bshape_register(lua_State* L){

	luaL_newmetatable(L, "phys.shape");//{m_physshape}
	lua_newtable(L);//{m_physshape},{}
	luaL_register(L,NULL,bshape_m);
	lua_setfield(L,-2,"__index");//{m_physshape}
	lua_pop(L,1);//

	lua_getglobal(L,"phys");//{}
	luaL_register(L,NULL,bshape_f);//{}
	lua_pop(L,1);//

	return 0;
}
