extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <btBulletDynamicsCommon.h>
#include <shared/lua_api/common.hpp>
#include "bcollider.hpp"


/*Collider things from lua have the form of:
{
	type = "ghost" | "multi" | "rigidbody" | "softbody"
	collider = ud_btCollisionObject,
	node = ud_ISceneNode, --Optional, on client
}
*/
btCollisionObject* popCollider(lua_State *L){
	lua_getfield(L,-1,"collider");
	btCollisionObject *r = (btCollisionObject*)lua_touserdata(L,-1);
	lua_pop(L,2);
	return r;
}

/***
Activates this object.
If this object was sleeping, it will move again. If you are using
applyforce or setvelocity, you will need to activate() the rigidbody for it
to move.
@function collider:activate()
*/
//collider:activate()
int activate(lua_State *L){
	btCollisionObject *r = popCollider(L);

	r->activate(true);

	return 0;
}

//collider:getfriction()
int getfriction(lua_State *L){
	btCollisionObject *r = popCollider(L);
	double fric = r->getFriction();
	lua_pushnumber(L, fric);
	return 1;
}

//collider:setfriction(number)
int setfriction(lua_State *L){
	double friction = lua_tonumber(L,-1);
	lua_pop(L,1);
	btCollisionObject *r = popCollider(L);
	r->setFriction(friction);
	return 0;
}

//collider:setpos({x,y,z})
int setpos(lua_State *L){
	double x,y,z;
	popvector3d(L,&x,&y,&z);
	lua_getfield(L,-1,"collider");
	btCollisionObject *c = (btCollisionObject*)lua_touserdata(L,-1);
	lua_pop(L,1);
	btTransform t = c->getWorldTransform();
	t.setOrigin(btVector3(x,y,z));
	c->setWorldTransform(t);
	c->activate();
	return 0;
}

//collider:getpos() :: {x,y,z}
int getpos(lua_State *L){
	lua_getfield(L,-1,"collider");
	btCollisionObject *c = (btCollisionObject*)lua_touserdata(L,-1);
	btTransform t = c->getWorldTransform();
	btVector3 o = t.getOrigin();
	pushvector3d(L,o.x(), o.y(), o.z());
	return 1;
}

extern const luaL_reg bcollider_m[] = {
	{"activate",         activate},
	{"getpos",           getpos},
	{"setpos",           setpos},
	{"getfriction",	     getfriction},
	{"setfriction",      setfriction},
	{NULL, NULL}
};
