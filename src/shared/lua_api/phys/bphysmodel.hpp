#ifndef _BPHYSMODEL_HPP_
#include <stdio.h>
#include <stdlib.h>
extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

int bphysmodel_register(lua_State* L);
void makebphysmodel(lua_State *L);
#endif
