#ifndef _BSHAPE_HPP_
#include <stdio.h>
#include <stdlib.h>
extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}

int bshape_register(lua_State* L);
#endif
