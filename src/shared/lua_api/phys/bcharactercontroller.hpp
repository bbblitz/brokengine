#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void bcharactercontroller_register(lua_State* L);
void makenewbcharactercontroller(lua_State* L);
extern const luaL_reg bcharactercontroller_m[];
