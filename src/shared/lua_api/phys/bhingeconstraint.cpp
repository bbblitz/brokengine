
#include <stdio.h>
#include <stdlib.h>
#include <list>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <shared/lua_api/common.hpp>
#include <btBulletDynamicsCommon.h>
#include <shared/lua_api/phys/bhingeconstraint.hpp>

extern btDiscreteDynamicsWorld* World;
extern std::list<btRigidBody*> Objects;

//newhingeconstraint(phys1,v3 axis, refrencephys1)
int newbhingeconstraint(lua_State *L){
	bool phys1 = lua_toboolean(L,-1) == 1;
	lua_pop(L,1);

	double x,y,z;
	popvector3d(L,&x,&y,&z);

	lua_getfield(L,-1,"rigidbody");
	btRigidBody *p1 = (btRigidBody*)lua_touserdata(L,-1);
	btTransform frame = p1->getCenterOfMassTransform();
	frame.setRotation(btQuaternion(x,y,z,0));
	lua_pop(L,2);

	btHingeConstraint(*p1,frame,phys1);
	printf("Done makeing new hinge constraint\n");

	return 0;
}

static const luaL_reg hingeconstraint_m[] = {
//	{"delete",	delbphysbox},//client side delete needs to delete the visual representation
	{0, 0},
};

void bhingeconstraint_register(lua_State* L){
	lua_getglobal(L,"phys");//{}
	lua_pushcfunction(L,newbhingeconstraint);//{},newhingeconstraint()
	lua_setfield(L,-2,"newhingeconstraint");//{}

	lua_pop(L,1);//

	luaL_newmetatable(L,"phys.hingeconstraint");
	lua_newtable(L);//phys.hingeconstraint,{}
	luaL_register(L,NULL,hingeconstraint_m);
	//luaL_register(L,NULL,cbphysbox_m);//phys.hingeconstraint,{}
	lua_setfield(L,-2,"__index");//phys.physbox

	lua_pop(L,1);

	//printf("When registering physbox, new() is %p\n",newcbphysbox);
	//printf("setpos is %p\n",cbphyssetpos);

	lua_pop(L,1);
}
