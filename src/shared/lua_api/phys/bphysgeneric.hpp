
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}


int setgravity(lua_State *L);
int applyforce(lua_State *L);
int getlineardamping(lua_State *L);
int getangulardamping(lua_State *L);
int setdamping(lua_State *L);
int activate(lua_State *L);
int getvelocity(lua_State *L);
int setvelocity(lua_State *L);

extern const luaL_reg brigidbody_m[];
