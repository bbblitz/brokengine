extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}
#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>

#include <shared/lua_api/common.hpp>

extern btDiscreteDynamicsWorld* World;

struct BActionItem: public btActionInterface{
	int ref;
	lua_State *L;
	void updateAction(btCollisionWorld *world, btScalar delta){
		lua_rawgeti(this->L,LUA_REGISTRYINDEX,this->ref);//{}
		pusherrorfunc(this->L);//{},errfunc()
		lua_getfield(this->L,-2,"action");//{},errfunc(),action()
		if(lua_isnil(this->L,-1)){//no .action method
			lua_pop(this->L,3);
			return;
		}
		lua_pushvalue(this->L,-3);//{},errfunc(),action(),{}
		lua_pushlightuserdata(this->L,world);//{},errfunc(),action(),{},ud_world
		lua_pushnumber(this->L,delta);//{},errfunc(),action(),{},ud_world,delta
		lua_pcall(this->L,3,0,-5);//{},errfunc()

		//printf("error:%d\n",err);
		lua_pop(this->L,2);
		return;
	}
	void debugDraw(btIDebugDraw *d){
		//no debug draw I guess
	}
};

int makeaction(lua_State *L){
	lua_newtable(L);//{}
	int r = luaL_ref(L,LUA_REGISTRYINDEX);
	lua_rawgeti(L,LUA_REGISTRYINDEX,r);
	BActionItem *a = new BActionItem();
	a->ref = r;
	a->L = L;
	World->addAction(a);
	lua_pushlightuserdata(L,a);//{},ud_action
	lua_setfield(L,-2,"action");//{}
	return 1;
}

int newaction(lua_State *L){
	lua_newtable(L);//{}
	return 0;
}

int baction_register(lua_State *L){
	lua_getglobal(L,"phys");
	lua_pushcfunction(L,makeaction);
	lua_setfield(L,-2,"makeaction");
	
	lua_pop(L,1);
	return 0;
}
