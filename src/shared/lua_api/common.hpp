#ifndef __broken_shared_lua_common
#define __broken_shared_lua_common

#define set_const(l,x) lua_pushstring(l,#x);lua_pushinteger(l,x);lua_settable(l,-3);
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

void loadLLibs(lua_State*);

int pushvector4i(lua_State*,long,long,long,long);
int pushvector3i(lua_State*,long,long,long);
int pushvector3d(lua_State*,double,double,double);
int pushvector2i(lua_State*,long,long);

int popvector4i(lua_State*,long*,long*,long*,long*);
int popvector4d(lua_State*,double*,double*,double*,double*);
int popvector3i(lua_State*,long*,long*,long*);
int popvector3d(lua_State*,double*,double*,double*);
int popvector2i(lua_State*,long*,long*);

int poprecti(lua_State* L,long*,long*,long*,long*);
int pushrecti(lua_State* L,long,long,long,long);

void pusherrorfunc(lua_State* L);
int make_crashy(lua_State* L);
#endif
