#ifndef __H_loadio
#define __H_loadio
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

void load_iofuncs(lua_State* L);
#endif

