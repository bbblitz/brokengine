extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace gui;
using namespace core;

extern IrrlichtDevice* device;

/***
Creates a new checkbox.
Creates a new checkbox that can be checked and unchecked. Checkboxes may have the
following fields, which they will call for callbacks:

	.onCheck(self)

It may additionally call any @{iguielement} callbacks

@function newcheckbox()
@tparam rect dimensions The rectangle to place the box at. If the box has a parent,
it is offset from the upper-left of the parent element.
@tparam string default_text The default text to have in the edit box
@tparam ?iguielement parent The parent element of the edit box
@treturn iguieditbox The edit box element
*/
//new({startx,starty},{endx,endy},"checkbox_name"[,ud_parent])
int newiguicheckbox(lua_State* L){
	IGUIElement* par = NULL;
	int nargs = lua_gettop(L);
	if(nargs == 3){
		lua_getfield(L,-1,"guielement");//{{sx,sy},{ex,ey}},"label",{parent},ud_parent
		par = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}},"chekboxname"
	}
	const char* text = lua_tostring(L,-1);//{{sx,sy},{ex,ey}},"label"
	//int tlen = strlen(text);
	lua_pop(L,1);//{startx,starty},{endx,endy}
	long startx,starty,endx,endy;
	poprecti(L,&startx,&starty,&endx,&endy);
	irr::gui::IGUICheckBox* cb = device->getGUIEnvironment()->addCheckBox(false,core::rect<int>(startx,starty,endx,endy),par,-1,stringw(text).c_str());
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,cb);//*checkbox
	lua_setfield(L,-2,"guielement");//{checkbox}
	luaL_getmetatable(L,"gui.checkbox");//{checkbox},m{gui.checkbox}
	lua_setmetatable(L,-2);//{checkbox}
	
	setelementcallback(L,EGET_CHECKBOX_CHANGED,"onCheck");
	
	return 1;
}

//ischecked(self)
int ischecked(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUICheckBox *e = (IGUICheckBox*)lua_touserdata(L,-1);
	lua_pop(L,2);
	bool checked = e->isChecked();
	lua_pushboolean(L,checked ? 1 : 0);
	return 1;
}

//setchecked(self, checked)
int setchecked(lua_State *L){
	int should = lua_toboolean(L,-1);
	lua_pop(L,1);
	lua_getfield(L,-1,"guielement");
	IGUICheckBox *e = (IGUICheckBox*)lua_touserdata(L,-1);
	lua_pop(L,2);
	e->setChecked(should == 1);
	return 0;
}

static const luaL_reg iguicheckbox_m[] = {
	{"ischecked",		ischecked},
	{"setchecked",		setchecked},
	{0,0},
};

int iguicheckbox_register(lua_State* L){//

	luaL_newmetatable(L,"gui.checkbox");//m{gui.checkbox}
	lua_newtable(L);
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguicheckbox_m);
	lua_setfield(L,-2,"__index");
	lua_pop(L,1);//

	lua_getglobal(L,"gui");//{gui}
	lua_pushstring(L,"newcheckbox");//{gui},new(),"newcheckbox"
	lua_pushcfunction(L,newiguicheckbox);//{gui},new()
	lua_settable(L,-3);//{gui}
	lua_pop(L,1);
	return 0;
}
