
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "iguilabel.hpp"
#include "../../callbackhandeler.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace gui;

extern IrrlichtDevice* device;

/***
@function newlabel()
Creates a new label to display text.
@tparam rect pos The position of the label, reletive to the upper-left of it's
parent element, or the root window if parent is nil.
@tparam string text The text to display on this label.
@tparam ?iguielement parent The parent element of the button.
@treturn iguilabel The label element
*/
//gui.newlabel({{sx,sy},{ex,ey}},"text"[,parent]) :: {guielement}
static int newiguilabel(lua_State* L){
	printf("Createing label!\n");
	int nargs = lua_gettop(L);
	IGUIElement* parent = NULL;
	if(nargs == 3){
		lua_getfield(L,-1,"guielement");//{{sx,sy},{ex,ey}},"text",{guielemtn=parent},parent
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}},"text"
	}
	//{{sx,sy},{ex,ey}},"text"

	const char* text = lua_tostring(L,-1);//{{sx,sy},{ex,ey}},"text"
	int bls = strlen(text);
	wchar_t* text_w = (wchar_t*)malloc((sizeof(wchar_t)*bls) + 1);//+1 for null
	mbstowcs(text_w,text,bls);
	text_w[bls] = L'\0';
	lua_pop(L,1);//{{sx,sy},{ex,ey}}

	long sx, sy, ex, ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIStaticText* statictext = env->addStaticText(
		text_w,
		core::rect<s32>(sx,sy,ex,ey),
		false,
		false,
		parent,
		-1,
		false
	);
	statictext->setWordWrap(true);

	lua_newtable(L); //{}
	lua_pushlightuserdata(L,statictext);//{},*statictext
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguilabel");//{guielement},{m_guielement}
	lua_setmetatable(L,-2);//{guielement}
	//registerguielement(L);
	return 1;
}

static const luaL_reg iguilabel_f[] = {
  {"newlabel",           newiguilabel},
  {0,0},
};

static const luaL_reg iguilabel_m[] = {
  {0, 0},
};

void iguilabel_register(lua_State* L){

	luaL_newmetatable(L, "gui.iguilabel");//{m_gui.iguilabel}
	lua_newtable(L);//{m_gui.iguilabel},{}

	luaL_register(L,NULL,iguilabel_m);//{m_gui.iguilabel},{guilabel}
	luaL_register(L,NULL,iguielement_m);
	lua_setfield(L,-2,"__index");//{m_gui.iguilabel}
	lua_pop(L,1);

	lua_getglobal(L,"gui");//{gui}
	luaL_register(L, NULL,iguilabel_f);//{gui}
	lua_pop(L,1);//
}
