#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <string>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/util/hashmap.hpp>
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice* device;

/***
@function newfileopendialog()
Creates a new dialog to open a file.
The file creation window may have the following fields set for callbacks:
	.onDirectorySelect(self)
	.onFileSelect(self)
	.onCanceled(self)
@tparam ?string title The rectangle to place the button at. If the box has a parent,
it is offset from the upper-left of the parent element.
@tparam ?string path The path to open the file dialog to
@tparam ?iguielement parent The parent element of the button.
@tparam ?boolean modal If other gui elements can be interacted with before this element is closed
@treturn iguifileopendialog The button element
*/
//gui.newfileopendialog(["title"][,"path"][,parent][,modal])
static int newfileopendialog(lua_State* L){
	wchar_t *title = NULL;// = L"File open dialog";
	io::path::char_type *path = NULL;// = L"";
	bool modal = true;
	IGUIElement* parent = NULL;

	int nargs = lua_gettop(L);
	if(nargs > 3){
		modal = lua_toboolean(L,-1) == 1;//"title","path",{parent},modal
		lua_pop(L,1);//"title","path",{parent}
	}
	if(nargs > 2){
		lua_getfield(L,-1,"guielement");//"title","path",{parent},ud_parent
		parent = (IGUIElement*)lua_touserdata(L,-1);//"title","path",{parent},ud_parent
		lua_pop(L,2);//"title","path"
	}
	if(nargs > 1){
		const char *pathc = lua_tostring(L,-1);//"title","path"
		lua_pop(L,1);//"title"
		path = (io::path::char_type*)pathc;
	}
	if(nargs > 0){
		const char *titlec = lua_tostring(L,-1);
		lua_pop(L,1);//
		size_t titlecslen = strlen(titlec);
		title = (wchar_t*)malloc(sizeof(wchar_t) * (titlecslen + 1));
		mbstowcs(title,titlec,titlecslen);
		title[titlecslen] = L'\0';
	}
	//printf("Got all arguments\n");
	IGUIEnvironment *env = device->getGUIEnvironment();
	IGUIFileOpenDialog *but = env->addFileOpenDialog(title,modal,parent,-1,false,path);
	//printf("Created file open dialog\n");
	//printf("Added file open dialog\n");
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,but);//{},ud_iguibutton
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguifileopendialog");//{guielement},{m_iguibutton}
	lua_setmetatable(L,-2);//{guielement}

	//printf("Created lua representation\n");
	setelementcallback(L,EGET_DIRECTORY_SELECTED,"onDirectorySelect");//
	setelementcallback(L,EGET_FILE_SELECTED,"onFileSelect");
	setelementcallback(L,EGET_FILE_CHOOSE_DIALOG_CANCELLED,"onCanceled");
	//printf("Finished registering callback\n");

	free(title);
	//printf("Freed everything\n");
	return 1;
}

//{fileopendialog} -> "dir"
int getdir(lua_State *L){
	lua_getfield(L,-1,"guielement");//{fileopendialog},ud_fileopendialog
	IGUIFileOpenDialog *f = (IGUIFileOpenDialog*)lua_touserdata(L,-1);
	lua_pop(L,2);
	const char *dname = f->getDirectoryName().c_str();
	lua_pushstring(L,dname);
	return 1;
}

//{fileopendialog} -> "name"
int getname(lua_State *L){
	lua_getfield(L, -1, "guielement");//{fileopendialog},ud_fileopendialog
	IGUIFileOpenDialog *f = (IGUIFileOpenDialog*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	const wchar_t *name_w = f->getFileName();
	size_t slen = wcslen(name_w);
	char name_c[slen + 1]; // + 1 for null
	wcstombs(name_c,name_w,slen);
	name_c[slen] = '\0';
	lua_pushstring(L,name_c);
	return 1;
}

static const luaL_reg iguifileopendialog_f[] = {
	{"new",             newfileopendialog},
	{0,0},
};

static const luaL_reg iguifileopendialog_m[] = {
	{"getdir",          getdir},
	{"getname",         getname},
	{0,0},
};

void iguidialog_register(lua_State* L){
	tL = L;

	luaL_newmetatable(L, "gui.iguifileopendialog");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguifileopendialog_m);//{m_iguibutton},{}
	lua_setfield(L,-2,"__index");//{m_iguibutton}

	lua_pop(L,1);

	lua_getglobal(L,"gui");
	lua_pushcfunction(L,newfileopendialog);
	lua_setfield(L,-2,"newfileopendialog");

	lua_pop(L,1);
}
