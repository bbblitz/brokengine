#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <string>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/util/hashmap.hpp>
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice* device;
char lhashkey[20];

/***
Creates a new button.
Buttons may have the following fields set for callbacks:
	.onClick(self)
	.onFocus(self)
	.onUnfocus(self)
	.onHover(self)
	.onLeave(self)
@function newbutton()
@tparam rect dimensions The rectangle to place the button at. If the box has a parent,
it is offset from the upper-left of the parent element.
@tparam string default_text The default text to have in the button
@tparam ?iguielement parent The parent element of the button.
@treturn iguibutton The button element
*/
//gui.newbutton({{sx,sy},{ex,ey}},"text"[,parent])
static int newiguibutton(lua_State* L){
	//printf("Createing gui button!\n");

	int nargs = lua_gettop(L);
	if(nargs > 3 || nargs < 2){
		lua_pop(L,nargs);
		lua_pushstring(L,"Incorrect number of arguments for gui.newbutton(rect,\"text\"[,parent])");
		lua_error(L);
	}
	IGUIElement* parent = NULL;
	if(nargs == 3){
		lua_getfield(L,-1,"guielement");
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}},"text"
	}
  	const char* label_c = lua_tostring(L,-1);//{{sx,sy},{ex,ey}},"text"
	size_t label_c_len = strlen(label_c);
	wchar_t label_w[label_c_len + 1] = L"";
	mbstowcs(label_w,label_c,label_c_len);
	label_w[label_c_len] = L'\0';
	lua_pop(L,1);//{{sx,sy},{ex,ey}}
	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	rect<s32> dim = rect<s32>(sx,sy,ex,ey);
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIButton* but = env->addButton(dim,parent,-1,label_w,L"");

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,but);//{},ud_iguibutton
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguibutton");//{guielement},{m_iguibutton}
	lua_setmetatable(L,-2);//{guielement}

	setelementcallback(L,EGET_BUTTON_CLICKED,"onClicked");//
	
	return 1;
}

static const luaL_reg iguibutton_f[] = {
	{"new",             newiguibutton},
	{0,0},
};

static const luaL_reg iguibutton_m[] = {
	{0,0},
};

void iguibutton_register(lua_State* L){
	tL = L;

	luaL_newmetatable(L, "gui.iguibutton");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguibutton_m);//{m_iguibutton},{}
	lua_setfield(L,-2,"__index");//{m_iguibutton}

	lua_pop(L,1);

	lua_getglobal(L,"gui");
	lua_pushcfunction(L,newiguibutton);
	lua_setfield(L,-2,"newbutton");

	lua_pop(L,1);
}
