

extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace core;
using namespace gui;


extern IrrlichtDevice* device;

/***
@function newtreeview()
Creates a new tree view
@tparam dimensions rect The rectangle to place the tree view at.
If the box has a parent, it is offset from the upper-left of the parent element.
@tparam ?iguielement parent The parent element of the button.
@tparam ?boolean draw_background Should we draw a background for the tree view?
@tparam ?boolean vertical_scroll Should there be a vertical scroll bar?
@tparam ?boolean horizonal_scroll Should there be a horizontal scroll bar?
@treturn iguitreeview The tree view element
*/
//gui.newtreeview({{sx,sy},{ex,ey}},[,parent][,draw_background][,vertical_scroll][,horizontal_scroll])
static int newiguitreeview(lua_State* L){
	//printf("Createing gui button!\n");

	int nargs = lua_gettop(L);
	IGUIElement* parent = NULL;
	int draw_background, scrollvertical, scrollhorizontal;
	if(nargs > 4){
		scrollhorizontal = lua_toboolean(L,-1);
		lua_pop(L,1);
	}
	if(nargs > 3){
		scrollvertical = lua_toboolean(L,-1);
		lua_pop(L,1);
	}
	if(nargs > 2){
		draw_background = lua_toboolean(L,-1);
		lua_pop(L,1);
	}
	if(nargs > 1){
		if(!lua_isnil(L,-1)){
			lua_getfield(L,-1,"guielement");
			parent = (IGUIElement*)lua_touserdata(L,-1);
			lua_pop(L,2);//{{sx,sy},{ex,ey}},"text"
		}
	}

	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	rect<s32> dim = rect<s32>(sx,sy,ex,ey);
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUITreeView* but = env->addTreeView(dim,parent,-1,draw_background,scrollvertical,scrollhorizontal);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,but);//{},ud_treeview
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguitreeview");//{guielement},{m_iguibutton}
	lua_setmetatable(L,-2);//{guielement}

	return 1;
}

//iguitreeview:getroot() :: treenode
int getRootNode(lua_State *L){
	lua_getfield(L,-1,"guielement");//{treeview},ud_TreeView
	IGUITreeView *v = (IGUITreeView*)lua_touserdata(L,-1);
	lua_pop(L,2);//

	IGUITreeViewNode *n = v->getRoot();//
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,n);//{},ud_treeviewnode
	lua_setfield(L,-2,"guielement");//{guielement=ud_treeviewnode}
	luaL_getmetatable(L,"gui.iguitreeviewnode");//{guielement=ud_treeviewnode},m_iguitreeviewnode
	lua_setmetatable(L,-2);//{treeviewnode}

	return 1;
}

//iguitreeviewnode:append_child("text"[,"icon"]) :: treenode
int appendChild(lua_State *L){
	const char *text_c = "";
	const char *icon_c = "";
	
	int nargs = lua_gettop(L);
	if(nargs > 2){
		icon_c = lua_tostring(L,-1);//{node},"text","icon"
		lua_pop(L,1);//{node},"text"
	}

	text_c = lua_tostring(L,-1);//{node},"text"
	lua_pop(L,1);//{node}

	printf("before getfield\n");
	lua_getfield(L,-1,"guielement");//{node},ud_node
	printf("after getfield\n");
	IGUITreeViewNode *v = (IGUITreeViewNode*)lua_touserdata(L,-1);//{node},ud_node
	lua_pop(L,2);//

	int slen = strlen(text_c);
	wchar_t text_w[slen + 1];
	mbstowcs(text_w,text_c,slen);
	text_w[slen] = L'\0';

	int slen2 = strlen(icon_c);
	wchar_t icon_w[slen2 + 1];
	mbstowcs(icon_w,icon_c,slen2);
	icon_w[slen2] = L'\0';

	printf("v is: %p\n",v);
	IGUITreeViewNode *tvn = v->addChildBack(text_w,icon_w,-1,-1,NULL,NULL);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,tvn);//{},ud_TreeViewNode
	lua_setfield(L,-2,"guielement");//{guielement=treeviewnode}
	luaL_getmetatable(L,"gui.iguitreeviewnode");//{treeviewnode},m_treeviewnode
	lua_setmetatable(L,-2);//{treeviewnode}

	return 1;
}

//iguitreeviewnode:append_child("text"[,"icon"])
int prependChild(lua_State *L){
	const char *text_c = "";
	const char *icon_c = "";
	
	int nargs = lua_gettop(L);
	if(nargs > 1){
		icon_c = lua_tostring(L,-1);//treeviewnode,"text","icon"
		lua_pop(L,1);//treeviewnode,"text"
	}

	text_c = lua_tostring(L,-1);//treeviewnode,"text"
	lua_pop(L,1);//treeviewnode

	printf("Before getfield\n");

	lua_getfield(L,-1,"guielement");//{treeviewnode},ud_element
	printf("After setfield\n");
	IGUITreeViewNode *v = (IGUITreeViewNode*)lua_touserdata(L,-1);//{treeviewnode},ud_element
	lua_pop(L,2);//

	int slen = strlen(text_c);
	wchar_t text_w[slen];
	mbstowcs(text_w,text_c,slen);

	int slen2 = strlen(icon_c);
	wchar_t icon_w[slen2];
	mbstowcs(icon_w,icon_c,slen2);

	IGUITreeViewNode *tvn = v->addChildFront(text_w,icon_w,-1,-1,NULL,NULL);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,tvn);//{},ud_TreeViewNode
	lua_setfield(L,-2,"guielement");//{guielement=treeviewnode}
	luaL_getmetatable(L,"gui.iguitreeviewnode");//{treeviewnode},m_treeviewnode
	lua_setmetatable(L,-2);//{treeviewnode}

	return 1;
}


static const luaL_reg iguitreeview_f[] = {
	{"newtreeview",             newiguitreeview},
	{0,0},
};

static const luaL_reg iguitreeview_m[] = {
	{"getroot",         getRootNode},
	{0,0},
};

static const luaL_reg iguitreeviewnode_m[] = {
	{"prepend_child",   prependChild},
	{"append_child",    appendChild},
	{0,0},
};

void iguitreeview_register(lua_State* L){
	tL = L;

	luaL_newmetatable(L, "gui.iguitreeview");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguitreeview_m);//{m_iguibutton},{}
	lua_setfield(L,-2,"__index");//{m_iguibutton}
	lua_pop(L,1);

	luaL_newmetatable(L, "gui.iguitreeviewnode");
	lua_newtable(L);
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguitreeviewnode_m);
	lua_setfield(L,-2,"__index");
	lua_pop(L,1);

	lua_getglobal(L,"gui");
	luaL_register(L,NULL,iguitreeview_f);
	lua_pop(L,1);
}
