#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "iguiwindow.hpp"
//#include "iguiutil.hpp"
#include "../../callbackhandeler.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace video;
using namespace gui;

extern IrrlichtDevice* device;
extern IGUIEnvironment* env;

/***
@function newiguiimage()
Creates a new guielement with an image.
The size of the iguielement is the same as the itexture used for it's creation.
@tparam vector2d position The upper-left hand corner of the element.
it is offset from the upper-left of the parent element.
@tparam number alpha The transparency of the element.
@tparam itexture texture The texture to display on this element.
@tparam ?iguielement parent The parent element of the button.
@treturn iguifileopendialog The button element
*/
//newiguiimage({startx,starty},alpha,{itexture}[,parent]) -> {guielement}
static int newiguiimage(lua_State* L){
	//printf("Creating iguiimage\n");
	IGUIElement *parent = NULL;
	if(lua_gettop(L) > 3){
		lua_getfield(L,-1,"guielement");
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);
	}
	lua_getfield(L,-1,"texture");//{startx,starty},alpha,{itexture},*itexture
	video::ITexture* tex = (video::ITexture*)lua_touserdata(L,-1);
	lua_pop(L,2);//{startx,starty},alpha,

	bool usealpha = lua_toboolean(L,-1);
	lua_pop(L,1);

	long sx,sy;
	popvector2i(L,&sx,&sy);
	
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIImage* img = env->addImage(tex,core::position2d<s32>(sx,sy),usealpha,parent,-1,L"");
	img->setImage(tex);
	
	lua_newtable(L);
	lua_pushlightuserdata(L,img);//ud_iguiimg
	lua_setfield(L,-2,"guielement");
	luaL_getmetatable(L,"iguiimage");//ud_iguiimg,{m_iguiimg}
	lua_setmetatable(L,-2);//ud_iguiimg

	return 1;
}

//setcolor(self,{r,g,b,a})
int setcolor(lua_State* L){
	long r,g,b,a;
	popvector4i(L,&r,&g,&b,&a);
	lua_getfield(L,-1,"guielement");
	IGUIImage *img = (IGUIImage*)lua_touserdata(L,-1);
	img->setColor(video::SColor(a,r,g,b));
	lua_pop(L,2);
	return 0;
}

//setimage(self,itexture)
int setimage(lua_State *L){
	lua_getfield(L,-1,"texture");//{iguiimg},{itex}
	ITexture* img = (ITexture*)lua_touserdata(L,-1);//{iguiimg},{itex},ud_itexture
	lua_pop(L,2);//{iguiimg}
	lua_getfield(L,-1,"guielement");//{iguiimg},ud_guiimg
	IGUIImage *gimg = (IGUIImage*)lua_touserdata(L,-1);//{iguiimg},ud_guiimg
	lua_pop(L,2);//
	gimg->setImage(img);
	return 0;
}

static const luaL_reg iguiimage_m[] = {
	{"setcolor",		setcolor},
	{"setimage",		setimage},
	{0, 0},
};

void iguiimage_register(lua_State* L){
	//printf("Loading iguiimage\n");
	luaL_newmetatable(L,"iguiimage");//{m_iguiimg}
	//printf("made meta table\n");
	lua_newtable(L);//{m_iguiimg},{}
	luaL_register(L,NULL,iguiimage_m);//{m_iguiimg},{iguiimg_m}
	luaL_register(L,NULL,iguielement_m);
	//printf("About to set field\n");
	lua_setfield(L,-2,"__index");//{m_iguiimg}
	lua_pop(L,1);//
	//printf("Got half way\n");	
	lua_getglobal(L,"gui");//{gui}
	lua_pushcfunction(L,newiguiimage);//{gui},newimg()
	lua_setfield(L,-2,"newiguiimage");//{gui}
	lua_pop(L,1);//
	//printf("Finished loading iguiimage\n");
}
