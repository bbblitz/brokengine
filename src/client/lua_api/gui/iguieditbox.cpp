
#include <stdio.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/util/hashmap.hpp>
#include <shared/lua_api/common.hpp>

/***
@module gui
*/

using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice* device;

/***
@function neweditbox()
Creates a new text entry box
@tparam rect dimensions The rectangle to place the box at. If the box has a parent, it is offset from the upper-left of the parent element.
@tparam ?iguielement parent The parent element of the edit box
@tparam ?string default_text The default text to have in the edit box
@treturn iguieditbox The edit box element
*/
//gui.neweditbox({{sx,sy},{ex,ey}}[,parent][,"default text"])
static int newiguieditbox(lua_State* L){
	printf("Creating edit box!\n");

	int nargs = lua_gettop(L);
	IGUIElement* parent = NULL;
	const wchar_t* defaulttext = L"";
	if(nargs >= 3){
		printf("Getting default text");
		const char* text_c = lua_tostring(L,-1);
		defaulttext = irr::core::stringw(text_c).c_str();
		lua_pop(L,1);//{{sx,sy},{ex,ey}}[,parent]
	}

	if(nargs >= 2){
		printf("Getting parent\n");
		lua_getfield(L,-1,"guielement");
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}}
	}

	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	rect<s32> dim = rect<s32>(sx,sy,ex,ey);
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIEditBox* eb = env->addEditBox(defaulttext,dim,true,parent,-1);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,eb);//{},ud_editbox
	lua_setfield(L,-2,"guielement");//{editbox}
	luaL_getmetatable(L,"gui.iguieditbox");//{editbox}{m_editbox}
	lua_setmetatable(L,-2);//{editbox}

	setelementcallback(L,EGET_EDITBOX_ENTER,"onEnter");
	setelementcallback(L,EGET_EDITBOX_CHANGED,"onChange");
	setelementcallback(L,EGET_EDITBOX_MARKING_CHANGED,"onMarkChange");

	printf("Done creating editbox\n");

	return 1;
}

//self:setmultiline(bool_enabled)
int set_multiline(lua_State *L){
	int should = lua_toboolean(L,-1);
	lua_pop(L,1);
	lua_getfield(L,-1,"guielement");
	IGUIEditBox *e = (IGUIEditBox*)lua_touserdata(L,-1);
	lua_pop(L,2);
	e->setMultiLine(should == 1);
	return 0;
}

static const luaL_reg iguieditbox_f[] = {
	{"neweditbox",newiguieditbox},
	{0,0},
};

static const luaL_reg iguieditbox_m[] = {
	{"set_multiline",set_multiline},
	{0,0},
};

void iguieditbox_register(lua_State* L){
	luaL_newmetatable(L, "gui.iguieditbox");
	lua_newtable(L);
	luaL_register(L,NULL,iguieditbox_m);
	luaL_register(L,NULL,iguielement_m);
	lua_setfield(L,-2,"__index");

	lua_pop(L,1);

	lua_getglobal(L,"gui");
	luaL_register(L,NULL,iguieditbox_f);
	lua_pop(L,1);
}
