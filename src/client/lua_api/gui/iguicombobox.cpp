extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/util/hashmap.hpp>
#include <shared/lua_api/common.hpp>
#include <client/lua_api/gui/iguicombobox.hpp>

/***
@module gui
*/
using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice* device;

/***
Creates a new combo box.
Buttons may have the following fields set for callbacks:
`.onChange(self)`
@function newcombobox()
@tparam rect dimensions The rectangle to place the button at. If the box has a parent,
it is offset from the upper-left of the parent element.
@tparam ?iguielement parent The parent element of the button.
@treturn iguicombobox The combo box element
*/
//gui.newcombobox({{sx,sy},{ex,ey}}[,parent])
static int newiguicombobox(lua_State* L){
	//printf("Createing gui button!\n");

	int nargs = lua_gettop(L);
	IGUIElement* parent = NULL;
	if(nargs == 2){
		lua_getfield(L,-1,"guielement");
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}}
	}

	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	rect<s32> dim = rect<s32>(sx,sy,ex,ey);
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUIComboBox* but = env->addComboBox(dim,parent,-1);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,but);//{},ud_iguicombobox
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguicombobox");//{guielement},{m_iguicombobox}
	lua_setmetatable(L,-2);//{guielement}

	setelementcallback(L,EGET_COMBO_BOX_CHANGED,"onChange");//
	
	return 1;
}

//{combobox}.addItem(self,text,id)
int additem(lua_State *L){
	int id = lua_tonumber(L,-1);
	lua_pop(L,1);//self,text
	const char *name_c = lua_tostring(L,-1);
	size_t name_c_len = strlen(name_c);
	printf("adding item to combo box: %s\n",name_c);
	wchar_t name_w[name_c_len + 1];
	mbstowcs(name_w,name_c,name_c_len);
	name_w[name_c_len] = L'\0';
	lua_pop(L,1);//self
	lua_getfield(L,-1,"guielement");//self,ud_guielement
	IGUIComboBox *box = (IGUIComboBox*)lua_touserdata(L,-1);
	box->addItem(name_w,id);
	lua_pop(L,2);//
	return 0;
}

// {combobox}.getselected(self)
int getselected(lua_State *L){
	lua_getfield(L,-1,"guielement");//self,ud_guielement
	IGUIComboBox *box = (IGUIComboBox*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	s32 sel = box->getSelected();
	lua_pushnumber(L,sel);//num_selected
	return 1;
}

//{combobox}.removeitem(self,id)
int removeitem(lua_State *L){
	int idx = lua_tonumber(L,-1);//self,id
	lua_pop(L,1);//self
	lua_getfield(L,-1,"guielement");//self,ud_guielement
	IGUIComboBox *box = (IGUIComboBox*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	box->removeItem(idx);
	return 0;
}

static const luaL_reg iguicombobox_f[] = {
	{"new",             newiguicombobox},
	{0,0},
};

static const luaL_reg iguicombobox_m[] = {
	{"addItem",         additem},
	{"getSelected",     getselected},
	{"removeItem",      removeitem},
	{0,0},
};

void iguicombobox_register(lua_State* L){
	tL = L;

	luaL_newmetatable(L, "gui.iguicombobox");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguicombobox_m);//{m_iguibutton},{}
	lua_setfield(L,-2,"__index");//{m_iguibutton}

	lua_pop(L,1);

	lua_getglobal(L,"gui");
	//luaL_register(L,NULL,iguicombobox_f)
	lua_pushcfunction(L,newiguicombobox);
	lua_setfield(L,-2,"newcombobox");

	lua_pop(L,1);
}
