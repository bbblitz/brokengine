#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "client/callbackhandeler.hpp"
#include <shared/util/hashmap.hpp>
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice* device;

/***
@function newspinbox()
Creates a new spinbox 
@tparam rect dimensions The rectangle to place the button at. If the box has a parent,
it is offset from the upper-left of the parent element.
@tparam ?string default_text The default text to display in the spinbox.
@tparam ?iguielement parent The parent element of the button.
@tparam ?boolean border Display a border around the spinbox
@treturn iguibutton The button element
*/
//gui.newspinbox({{sx,sy},{ex,ey}}[,"text"][,parent][,border])
static int newiguispinbox(lua_State* L){
	printf("Createing gui spinbox!\n");
	int nargs = lua_gettop(L);
	printf("Nargs was %d\n",nargs);

	IGUIElement* parent = NULL;
	const char *label_c = "";
	int border = 1;
	if(nargs > 3){
		border = lua_toboolean(L,-1);
		lua_pop(L,1);
	}
	if(nargs > 2){
		lua_getfield(L,-1,"guielement");
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);//{{sx,sy},{ex,ey}},"text"
	}
	if(nargs > 1){
		label_c = lua_tostring(L,-1);
		lua_pop(L,1);
	}
	
	size_t labellen = strlen(label_c);
	wchar_t label_w[labellen + 1]; //+1 for null
	mbstowcs(label_w,label_c,labellen);
	label_w[labellen] = L'\0';
	//const wchar_t* label_w = irr::core::stringw(label_c).c_str();//{{sx,sy},{ex,ey}},"text"
	//lua_pop(L,1);//{{sx,sy},{ex,ey}}
	printf("Created title\n");
	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	printf("Got coords\n");
	rect<s32> dim = rect<s32>(sx,sy,ex,ey);
	IGUIEnvironment* env = device->getGUIEnvironment();
	IGUISpinBox* but = env->addSpinBox(label_w,dim,border,parent,-1);

	printf("Added spinbox\n");
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,but);//{},ud_iguibutton
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguispinbox");//{guielement},{m_iguibutton}
	lua_setmetatable(L,-2);//{guielement}

	printf("Created lua representation\n");
	setelementcallback(L,EGET_SPINBOX_CHANGED,"onChanged");//
	printf("Finished registering callback\n");
	
	return 1;
}

static const luaL_reg iguispinbox_f[] = {
	{"new",             newiguispinbox},
	{0,0},
};

static const luaL_reg iguispinbox_m[] = {
	{0,0},
};

void iguispinbox_register(lua_State* L){
	tL = L;

	luaL_newmetatable(L, "gui.iguispinbox");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguispinbox_m);//{m_iguibutton},{}
	lua_setfield(L,-2,"__index");//{m_iguibutton}

	lua_pop(L,1);

	lua_getglobal(L,"gui");
	lua_pushcfunction(L,newiguispinbox);
	lua_setfield(L,-2,"newspinbox");

	lua_pop(L,1);
}
