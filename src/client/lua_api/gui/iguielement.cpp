/*This file defines some things that all igui stuff can do*/
/***
All gui elements inherit from iguielement.
Some functions (like settext()) do different things for different elements.
All gui elements can call the following callbacks:

	onFocus(self)
	onUnfous(self)
	onHover(self)
	onLeave(self)

@classmod iguielement
*/
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

#include <stdio.h>
#include <shared/lua_api/common.hpp>
#include <irrlicht.h>
#include "../guiparts.hpp"
#include <client/callbackhandeler.hpp>

using namespace irr;
using namespace core;
using namespace gui;

extern IrrlichtDevice *device;
/***
Move an element (by an offset) from it's current position
@function guielement:move()
@tparam vector2d position The offset to move this element by
*/
//move({element},{x,y}) -> nil
int moveiguielement(lua_State* L){
	//printf("Got call to move element\n");
	long x,y;
	popvector2i(L,&x,&y); //{element}
	//printf("I want to move to %d %d\n",x,y);
	lua_getfield(L,-1,"guielement");//{element},*element
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);
	//printf("Found element to move: %p\n",el);
	lua_pop(L,2);//

	el->move(position2d<s32>(x,y));
	el->updateAbsolutePosition();
	return 0;
}

/***
Set the visibility of this element
@function guielement:setvisible()
@tparam boolean visible Should this element be visible?
*/
int setvisible(lua_State *L){
	int v = lua_toboolean(L,-1);
	lua_pop(L,1);

	lua_getfield(L,-1,"guielement");
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);//{element},ud_element
	lua_pop(L,2);

	el->setVisible(v == 1);

	return 0;
}

/***
Find the rectangle that an element occupies
@function guielement:getabsrect()
@treturn rect The rectangle that this element occupies
*/
//getabsrect({element})-> {{sx,sy},{ex,ey}}
int getiguiclippingrect(lua_State* L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);//{element},ud_element
	lua_pop(L,2);
	core::rect<s32> rect = el->getAbsolutePosition();
	pushrecti(
		L,
		rect.UpperLeftCorner.X,
		rect.UpperLeftCorner.Y,
		rect.LowerRightCorner.X,
		rect.LowerRightCorner.Y
	);
	return 1;
}


/***
Find the rectangle that an element occupies that is visible to the user
@function guielement:getabsclippingrect()
@treturn rect The rectangle that this element occupies that is visible to the user
*/
//getabsclippingrect({element}) :: {{sx,sy},{ex,ey}}
int getiguiabsclippingrect(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);//{element},ud_element
	lua_pop(L,2);
	core::rect<s32> rect = el->getAbsoluteClippingRect();
	pushrecti(
		L,
		rect.UpperLeftCorner.X,
		rect.UpperLeftCorner.Y,
		rect.LowerRightCorner.X,
		rect.LowerRightCorner.Y
	);
	return 1;
}

/***
Find the relative rectangle that this element occupies
@function guielement:getrelrect()
@treturn rect The rectangle that this element occupies relative to it's parent
*/
int getiguirelrect(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);//{element},ud_element
	lua_pop(L,2);
	core::rect<s32> rect = el->getRelativePosition();
	pushrecti(
		L,
		rect.UpperLeftCorner.X,
		rect.UpperLeftCorner.Y,
		rect.LowerRightCorner.X,
		rect.LowerRightCorner.Y
	);
	return 1;
}

/***
Sets the text of the element
This function may do different things to different gui elements.
For example, on a window, it sets the title.
On a button, it sets the button's text.
@function guielement:settext()
@tparam string text The text to set on the element
*/
//setText({guielement},"text") :: nil
int setiguitext(lua_State* L){
	const char* text = lua_tostring(L, -1);
	lua_pop(L,1);//{guielement}
	lua_getfield(L,-1,"guielement");//{guielement},ud_iguielement
	IGUIElement* el = (IGUIElement*)lua_touserdata(L,-1);//{guielement},ud_iguielement
	lua_pop(L,2);
	int textlen = strlen(text);
	wchar_t* wtext = (wchar_t*)malloc(sizeof(wchar_t)*textlen + 1);//+1 for null?
	mbstowcs(wtext,text,textlen);
	wtext[textlen] = '\0';
	el->setText(wtext);
	free(wtext);
	return 0;
}

//setRect({guielement},{{sx,sy},{ex,ey}}) :: nil
int setrelrect(lua_State *L){
	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);
	printf("Seting rect %ld %ld %ld %ld\n",sx,sy,ex,ey);
	lua_getfield(L,-1,"guielement");//{guielement},ud_element
	IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);
	el->setRelativePosition(rect<s32>(sx,sy,ex,ey));
	return 0;
}


/***
@function guielement:gettext()
@treturn string The caption text of the element. For input element like
editboxes, this returns the text that the edit box contains.
*/
//{guieditbox}:gettext() :: "caption_text"
int getiguitext(lua_State* L){
	lua_getfield(L, -1, "guielement");//{guieditbox},ud_guielement
	irr::gui::IGUIElement *el = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	const wchar_t *t = el->getText();
	size_t cstrlen = wcslen(t);
	//printf("In gui get text, cstrlen is %zu\n",cstrlen);
	char output[cstrlen + 1];//+1 for \0
	wcstombs(output,t,cstrlen);
	output[cstrlen] = '\0';
	lua_pushstring(L,output);//"str"
	return 1;
}

/***
Removes a gui element, and any child elements
@function guielement:remove()
*/
//remove({self})
int removeiguielement(lua_State* L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	ele->remove();
	lua_pop(L,2);
	return 0;
}

/***
Focuses a gui element.
Will cause a ELEMENT_FOCUS_LOST followed by an ELEMENT_FOCUSED event.
If either events are trapped/returned from, then the focus will not change.
@function iguielement:focus()
*/
//focus({self})
int focus(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	device->getGUIEnvironment()->setFocus(ele);
	lua_pop(L,2);
	return 0;
}

//{guielement},bool
int setelementenabled(lua_State *L){
	int enable = lua_toboolean(L,-1);
	lua_pop(L,1);
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	ele->setEnabled(enable == 1);
	lua_pop(L,1);
	return 0;
}

//{guielement}
int getelementenabled(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	bool enabled = ele->isEnabled();
	lua_pushboolean(L,enabled?1:0);
	lua_pop(L,1);
	return 1;
}

//{guielement},{guielement}
int sendtofront(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);//{guielement}
	
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele2 = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	ele->bringToFront(ele2);
	return 0;
}

//{guielement},{guielement}
int sendtoback(lua_State *L){
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);//{guielement}
	
	lua_getfield(L,-1,"guielement");
	IGUIElement *ele2 = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	ele->sendToBack(ele2);
	return 0;
}

class guicallback{
private:
	int luaitem;
	const char* funcname;
	lua_State* L;
public:
	guicallback(lua_State* L, int lf, const char* fn){
		this->luaitem = lf;
		this->L = L;
		this->funcname = fn;
	}
	bool operator () (SEvent e) {
		lua_rawgeti(L,LUA_REGISTRYINDEX,this->luaitem);//{guielement}
		lua_getfield(L,-1,this->funcname);//{guielement},func()
		if(!lua_isnil(L,-1)){
			lua_rawgeti(L,LUA_REGISTRYINDEX,this->luaitem);//{guielement},func(),{guielement}
			lua_call(L,1,0);//{iguielement}
			lua_pop(L,1);//{}
			return true;
		}else{
			lua_pop(L,2);//{}
			return false;
		}
	}
};

//{guielement}
//popelementcallback(lua_State* L, gui::EGUI_EVENT_TYPE, char*)
void setelementcallback(lua_State* L, gui::EGUI_EVENT_TYPE et, const char* funcname){
	registerguielement(L,et,funcname);
}

//ud_iguielement
int guigetid(lua_State* L){
	IGUIElement* el = (IGUIElement*)lua_touserdata(L,-1);
	lua_pop(L,1);
	int id = el->getID();
	lua_pushnumber(L,id);
	return 1;
}


extern const luaL_reg iguielement_m[] = {
	{"move",         moveiguielement},
	{"setvisible",   setvisible},
	{"setrect",      setrelrect},
	{"getabsrect",   getiguiclippingrect},
	{"getabsclippingrect", getiguiabsclippingrect},
	{"getrelrect",   getiguirelrect},
	{"settext",      setiguitext},
	{"gettext",      getiguitext},
	{"remove",       removeiguielement},
	{"focus",        focus},
	{"enable",       setelementenabled},
	{"sendfront",    sendtofront},
	{"sendback",     sendtoback},
	{NULL, NULL}
};
