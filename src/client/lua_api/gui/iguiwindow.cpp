#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include "iguiwindow.hpp"
//#include "iguiutil.hpp"
#include "../../callbackhandeler.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace gui;

/***
@function newwindow()
Creates a new gui window .
@tparam rect dimensions The rectangle to create the window at.
@tparam string title_text The text to show in the title bar of the window
@tparam ?iguielement parent The parent element of the window.
@treturn iguiwindow The window element

In return to the usual gui element calls, IGUI window may call a `onClose()` callback.
*/
//new({{sx,sy},{ex,ey}},"title"[,{guielement=parent}]) :: {guielement}
static int newiguiwindow(lua_State* L){
	IGUIElement* parent = NULL;
	int numargs = lua_gettop(L);

	if(numargs == 4){
		lua_getfield(L,-1,"guielement");//{{sx,sy},{ex,ey}},"title",{guielement=parent},parent
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);
	}
	//{{sx,sy},{ex,ey},"title"
  	const char* title_c = lua_tostring(L,-1);
	const wchar_t* title_w = irr::core::stringw(title_c).c_str();
	lua_pop(L,1);//{{sx,sy},{ex,ey}}

	//Frame position
	long sx,sy,ex,ey;
	poprecti(L,&sx,&sy,&ex,&ey);//

	//Create the window
	IGUIEnvironment* env = guidevice->getGUIEnvironment();
	IGUIWindow* wi = env->addWindow(
		core::rect<s32>(sx,sy,ex,ey),
		false,
		title_w,
		parent,
		-1
	);

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,wi);//{},{ud_window}
	lua_setfield(L,-2,"guielement");//{element=ud_window}

	luaL_getmetatable(L,"gui.window");//{element=ud_window},{m_gui.window}
	lua_setmetatable(L,-2);//{element=ud_window, __meta=gui.window}

	int ref = luaL_ref(L,LUA_REGISTRYINDEX);//
	lua_rawgeti(L,LUA_REGISTRYINDEX,ref);//{element=ud_window, __meta=gui.window}
	registerguielement(L,EGET_ELEMENT_CLOSED,"onClose");
	iguielements[wi] = ref;

	//registerguicallback(wi,EGET_ELEMENT_CLOSED,iguiwindowevent);
	return 1;
}

static const luaL_reg iguiwindow_m[] = {
//	bool :: iguiwindow:close() -- Called when window is closed, returning
//	                           -- Anything but false or nil prevents close
	{0, 0},
};

int iguiwindow_register(lua_State* L, IrrlichtDevice* d){
	luaL_newmetatable(L,"gui.window");//m{gui.window}
	lua_newtable(L);
	luaL_register(L,NULL,iguiwindow_m);
	luaL_register(L,NULL,iguielement_m);
	lua_setfield(L,-2,"__index");
	lua_pop(L,1);//

	lua_getglobal(L,"gui");
	lua_pushcfunction(L,newiguiwindow);
	lua_setfield(L,-2,"newwindow");
	lua_pop(L,1);

	return 0;
}
