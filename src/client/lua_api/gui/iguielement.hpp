#ifndef __H_BROKEN_IGUIELEMENT
#define __H_BROKEN_IGUIELEMENT
#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

int moveiguielement(lua_State* L);
int setvisible(lua_State *L);
int getiguiclippingrect(lua_State* L);
int getiguiabsclippingrect(lua_State *L);
int getiguirelrect(lua_State *L);
int setiguitext(lua_State* L);
int getiguitext(lua_State* L);
int removeiguielement(lua_State* L);
void setelementcallback(lua_State* L, irr::gui::EGUI_EVENT_TYPE et, const char* funcname);
int guigethandeler(lua_State* L);
int guisethandeler(lua_State* L);
int guigetid(lua_State* L);
int setrelrect(lua_State *L);

extern const luaL_reg iguielement_m[];
#endif
