
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <string>
#include <irrlicht.h>
#include "../guiparts.hpp"
#include "iguielement.hpp"
#include <shared/lua_api/common.hpp>

/***
@module gui
*/
using namespace irr;
using namespace gui;
using namespace core;

extern IrrlichtDevice* device;

/***
@function newcolorselector()
Creates a new checkbox
@tparam string title The title for this color selector
@tparam ?iguielement parent The parent element of the edit box
@treturn iguicolorselector The color selector element
*/
//newcolorselector("title"[,parent])
int newiguicolorselector(lua_State* L){
	int nargs = lua_gettop(L);
	IGUIElement* parent = NULL;
	if(nargs == 2){
		lua_getfield(L,-1,"guielement");//"title",{parent},ud_element
		parent = (IGUIElement*)lua_touserdata(L,-1);
		lua_pop(L,2);
	}

	const char *title = lua_tostring(L,-1);//"title"
	lua_pop(L,1);//
	int titlelen = strlen(title);
	wchar_t titletext[titlelen];
	mbstowcs(titletext,title,titlelen);
	IGUIElement* colorselector = device->getGUIEnvironment()->addColorSelectDialog(
		titletext,
		false,
		parent,
		-1
	);
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,colorselector);//{},ud_colorselector
	lua_setfield(L,-2,"guielement");//{guielement}
	
	luaL_getmetatable(L,"gui.colorselector");//{guielement}{m_colorselector}
	lua_setmetatable(L,-2);//{colorselector}

	return 1;
}

static const luaL_reg iguicolorselector_m[] = {
	{0,0},
};

int iguicolorselector_register(lua_State* L){//
	luaL_newmetatable(L,"gui.colorselector");//m{gui.checkbox}
	luaL_register(L,NULL,iguielement_m);
	luaL_register(L,NULL,iguicolorselector_m);
	lua_pop(L,1);//

	lua_getglobal(L,"gui");//{gui}
	lua_pushstring(L,"newcolorselector");//{gui},new(),"newcheckbox"
	lua_pushcfunction(L,newiguicolorselector);//{gui},new()
	lua_settable(L,-3);//{gui}
	lua_pop(L,1);
	return 0;
}
