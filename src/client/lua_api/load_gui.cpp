/***
Utilities for drawing GUI things on the screen
@module gui
*/

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

#include "gui/iguibutton.hpp"
#include "gui/iguilabel.hpp"
#include "gui/iguiwindow.hpp"
#include "gui/iguicheckbox.hpp"
#include "gui/iguiimage.hpp"
#include "gui/iguieditbox.hpp"
#include "gui/iguicolorselector.hpp"
#include "gui/iguifiledialog.hpp"
#include "gui/iguispinbox.hpp"
#include "gui/iguitreeview.hpp"
#include "gui/iguicombobox.hpp"
#include "gui/iguielement.hpp"
#include "../callbackhandeler.hpp"
#include "guiparts.hpp"

using namespace irr;
using namespace gui;
using namespace core;

extern IrrlichtDevice* device;

//Things from guiparts.hpp
std::map<irr::gui::IGUIElement*,int> iguielements;
IrrlichtDevice* guidevice;
//long gui_elenum;
//std::vector<irr::gui::IGUIElement*> guielements(1);
lua_State* tL;

int screenwidth(lua_State* L);
int screenheight(lua_State* L);
int getroot(lua_State *L);

/***
@function gui.scrw()
@treturn number The width of the screen
*/

/***
@function gui.scrh()
@treturn number The height of the screen
*/
void load_guifuncs(lua_State* L){
	printf("Started loading gui...\n");
	tL = L;
	guidevice = device;

	IGUIFont *font = device->getGUIEnvironment()->getFont("../data/res/font.xml");
	//Set the transparency of new igui windows
	IGUISkin *skin = device->getGUIEnvironment()->getSkin();
	for(int i = 0; i < EGDC_COUNT; i++){
		video::SColor col = skin->getColor((EGUI_DEFAULT_COLOR)i);
		col.setAlpha(255);
		skin->setColor((EGUI_DEFAULT_COLOR)i, col);
	}
	for(int i = 0; i < EGDF_COUNT; i++){
		skin->setFont(font,(EGUI_DEFAULT_FONT)i);
	}
	//Various enums

	lua_newtable(L);

	lua_pushcfunction(L,getroot);
	lua_setfield(L,-2,"getroot");

	lua_setglobal(L,"gui");
	
	iguilabel_register(L);
	iguicheckbox_register(L);
	iguiwindow_register(L,device);
	iguiimage_register(L);
	iguibutton_register(L);
	iguicolorselector_register(L);
	iguidialog_register(L);
	iguispinbox_register(L);
	iguitreeview_register(L);
	iguieditbox_register(L);
	iguicombobox_register(L);

	lua_pushcfunction(L,screenwidth);
	lua_setglobal(L,"scrw");

	lua_pushcfunction(L,screenheight);
	lua_setglobal(L,"scrh");

	luaL_newmetatable(L, "gui.iguielement");//{m_iguielement}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,iguielement_m);
	lua_setfield(L,-2,"__index");//{m_iguielement}
	lua_pop(L,1);

}


int screenheight(lua_State* L){
	core::rect<s32> dim = guidevice->getGUIEnvironment()->getRootGUIElement()->getAbsoluteClippingRect();
	lua_pushnumber(L,dim.getHeight());
	//printf("Got screen height:%d\n",dim.getWidth());
	return 1;
}

int screenwidth(lua_State* L){
	core::rect<s32> dim = guidevice->getGUIEnvironment()->getRootGUIElement()->getAbsoluteClippingRect();
	lua_pushnumber(L,dim.getWidth());
	//printf("Got screen width:%d\n",dim.getWidth());
	return 1;
}

//Get the root gui element
int getroot(lua_State* L){
	IGUIElement *ele = device->getGUIEnvironment()->getRootGUIElement();
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,ele);//{},ud_iguibutton
	lua_setfield(L,-2,"guielement");//{guielement}
	luaL_getmetatable(L,"gui.iguielement");//{guielement},{m_iguibutton}
	lua_setmetatable(L,-2);//{guielement}

	return 1;
}
