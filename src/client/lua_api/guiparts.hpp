#ifndef __H_guiparts
#define __H_guiparts
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <string>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../util/hashmap.h"


typedef struct LIGUIElement {
  irr::gui::IGUIElement* e;
  map_t funcmap;
  const char* type;
} LIGUIElement;

extern lua_State* tL;
extern irr::IrrlichtDevice* guidevice;
extern long gui_elenum;
extern std::vector<irr::gui::IGUIElement*> guielements;
extern std::map<irr::gui::IGUIElement*,int> iguielements;
#endif
