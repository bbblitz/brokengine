
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <assert.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

#include <btBulletDynamicsCommon.h>
#include <irrlicht.h>
#include "cbphysbox.hpp"
#include "../scene/imesh.hpp"
#include <shared/lua_api/phys/bphysbox.hpp>
#include <shared/lua_api/phys/bcollider.hpp>
#include "../scene/igeneric.hpp"
#include <shared/lua_api/common.hpp>

#include <shared/lua_api/phys/bphysgeneric.hpp>

using namespace irr;
using namespace scene;
using namespace core;
using namespace video;

extern IrrlichtDevice* device;

extern btDiscreteDynamicsWorld* World;
extern std::list<btRigidBody*> Objects;


//phys.newphysbox({vector3 size},{vector3 origin},mass)
static int newcbphysbox(lua_State* L){//
	//printf("Createing new cbphysbox\n");
	double sx,sy,sz,x,y,z,mass;
	//Get the data
	mass = lua_tonumber(L,-1);//{v3 size}, {v3 origin}, mass
	lua_pop(L,1);//{v3 size}, {v3 origin}
	popvector3d(L,&x,&y,&z);//{v3 size}
	popvector3d(L,&sx,&sy,&sz);//

	pushvector3d(L,sx,sy,sz);//{v3 size}
	pushvector3d(L,x,y,z);//{v3 size},{v3 origin}
	lua_pushnumber(L,mass);//{v3 size}, {v3 origin}, mass	
	makenewbphysbox(L);//ud_btRigidbody
	btRigidBody* r = (btRigidBody*)lua_touserdata(L,-1);//ud_btRigidbody
	lua_pop(L,1);

	pushvector3d(L,sx*2,sy*2,sz*2);//{v3 size}
	pushvector3d(L,x,y,z);//{v3 size},{v3 origin}
	makenewiscenecube(L);//ud_iscenenode
	ISceneNode* n = (ISceneNode*)lua_touserdata(L,-1);//ud_iscenenode
	lua_pop(L,1);//

	r->setUserPointer(n);

	lua_newtable(L);//{}

	lua_pushlightuserdata(L,r);//{},ud_rigidbody
	lua_setfield(L,-2,"collider");//{}
	lua_pushstring(L,"rigidbody");//{},"rigidbody"
	lua_setfield(L,-2,"type");//{}
	lua_pushlightuserdata(L,n);//{},ud_iscenenode
	lua_setfield(L,-2,"node");//{}
	
	lua_getglobal(L,"phys");//{rb},{phys}
	lua_getfield(L,-1,"colliders");//{rb},{phys},{phys.colliders}
	lua_pushlightuserdata(L,r);//{rb},{phys},{colliders},ud_rb
	lua_pushvalue(L,-4);//{rb},{phys},{colliders},ud_rb,{rb}
	lua_settable(L,-3);//{rb},{phys},{phys.colliders}
	lua_pop(L,2);//{rb}

	luaL_getmetatable(L,"phys.physbox");//{},{phys.physbox}
	lua_setmetatable(L,-2);//{}

	return 1;
}

//bphysbox:setpos({v3 pos})
int cbphyssetpos(lua_State* L){//{rigidbody=ud_btRigidbody,node=ud_iscenenode},{v3 pos}
	//printf("calling cbphysbox setpos\n");
	double x,y,z;
	popvector3d(L,&x,&y,&z);//{rigidbody=ud_btRigidbody,node=ud_iscenenode}

	//printf("Getting rigidbody\n");
	lua_getfield(L,-1,"collider");//{rigidbody=ud_btRigidbody,node=ud_iscenenode},ud_btRigidbody
	btRigidBody* r = (btRigidBody*)lua_touserdata(L,-1);//{rigidbody=ud_btRigidbody,node=ud_iscenenode},ud_btRigidbody
	//printf("Got rigidbody, it was %p\n",r);
	lua_pop(L,1);//{rigidbody=ud_btRigidbody,node=ud_iscenenode}
	lua_getfield(L,-1,"node");//{rigidbody=ud_btRigidbody,node=ud_iscenenode},ud_iscenenode
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);//{btRigidBody=ud_btRigidbody,node=ud_iscenenode},ud_iscenenode
	//printf("Got node, it was %p\n",i);
	lua_pop(L,2);//

	btTransform bt = btTransform(btQuaternion(0,0,0,1), btVector3(x,y,z));
	btMotionState* ms = r->getMotionState();
	ms->setWorldTransform(bt);
	r->setWorldTransform(bt);
	
	r->activate(true);

	i->setPosition(vector3df(x,y,z));
	i->updateAbsolutePosition();

	return 0;
	
}

//bphysbox:getpos()
int cbphysgetpos(lua_State* L){//{rigidbody=ud_btRigidbody,node=ud_iscenenode}
	//printf("cphysgetpos called, stack size is %d\n",lua_gettop(L));
	lua_getfield(L,-1,"collider");//{rigidbody=ud_btRigidbody,node=ud_iscenenode}, ud_rigidbody
	btRigidBody* r = (btRigidBody*) lua_touserdata(L,-1);
	lua_pop(L,2);
	btTransform bt = r->getCenterOfMassTransform();
	btVector3 p = bt.getOrigin();
	pushvector3d(L,p.x(),p.y(),p.z());
	return 1;
}

int cbphysgetgravity(lua_State* L){
	lua_getfield(L,-1,"collider");//{rigidbody=ud_btRigidbody,node=ud_iscenenode}, ud_rigidbody
	btRigidBody* r = (btRigidBody*) lua_touserdata(L,-1);
	lua_pop(L,2);
	btVector3 p = r->getGravity();
	pushvector3d(L,p.x(),p.y(),p.z());
	return 1;
}

int cbphysapplygravity(lua_State* L){
	lua_getfield(L,-1,"collider");//{rigidbody=ud_btRigidbody,node=ud_iscenenode}, ud_rigidbody
	btRigidBody* r = (btRigidBody*) lua_touserdata(L,-1);
	lua_pop(L,2);
	r->applyGravity();
	return 0;
}

//setMaterial(self,material)
int cbsetmaterial(lua_State* L){
	printf("Call to setmaterial\n");
	//SMaterial* mat = (SMaterial*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_IMaterial
	ITexture* tex = (ITexture*)lua_touserdata(L,-1);
	lua_pop(L,1);//{node=ud_ISceneNode}
	printf("About to get field node\n");
	lua_getfield(L,-1,"node");//{node=ud_ISceneNode},ud_ISceneNode
	printf("After call to field node\n");
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);//

	lua_pushlightuserdata(L,i);
	lua_pushlightuserdata(L,tex);
	printf("Finished getting everything for setmaterial\n");
	iscenesetmaterial(L);

	return 0;
}

static const luaL_reg cbphysbox_m[] = {
	{"setpos",	cbphyssetpos},//overload
	{"getpos",	cbphysgetpos},
	{0, 0},
};

void cbphysbox_register(lua_State* L){
	assert(lua_gettop(L) == 0);
	bphysbox_register(L);//
	assert(lua_gettop(L) == 0);
	lua_getglobal(L,"phys");//{}
	lua_pushcfunction(L,newcbphysbox);//{},newcbphysbox()
	lua_setfield(L,-2,"newphysbox");//{phys}

	lua_pop(L,1);//
	assert(lua_gettop(L) == 0);
	
	luaL_getmetatable(L,"phys.physbox");//phys.physbox
	lua_newtable(L);//phys.physbox,{}
	luaL_register(L,NULL,bcollider_m);
	luaL_register(L,NULL,brigidbody_m);
	luaL_register(L,NULL,igeneric_m);
	luaL_register(L,NULL,cbphysbox_m);//phys.physbox,{}
	lua_pushstring(L,"rigidbody");//phys.physbox,{},"rigidbody"
	lua_setfield(L,-2,"type");//phys.physbox,{}
	lua_setfield(L,-2,"__index");//phys.physbox

	lua_pop(L,1);
	//printf("When registering physbox, new() is %p\n",newcbphysbox);
	//printf("setpos is %p\n",cbphyssetpos);


	assert(lua_gettop(L) == 0);
}
