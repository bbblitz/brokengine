
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <list>
#include <assert.h>
extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}
#include <btBulletDynamicsCommon.h>
#include <irrlicht.h>
#include "../gameparts.hpp"
#include "cbphysbox.hpp"
#include "cbphysmodel.hpp"
#include <client/lua_api/scene/igeneric.hpp>
#include <shared/lua_api/phys/bphysmodel.hpp>
#include <shared/lua_api/phys/bcollider.hpp>
#include <shared/lua_api/phys/bphysgeneric.hpp>
#include <shared/lua_api/common.hpp>
#include <shared/util/tinyobj.hpp>


using namespace irr;
using namespace scene;
using namespace core;
using namespace video;

extern IrrlichtDevice* device;

extern btDiscreteDynamicsWorld* World;
extern std::list<btRigidBody*> Objects;

//newbphysmodel("graphicfile","physicfile",mass,[,{position}][,{lookat}])
static int newbphysmodel(lua_State* L){
	//printf("Creating cbphysmodel\n");
	int nargs = lua_gettop(L);
	double lx,ly,lz;
	double x,y,z;
	if(nargs > 4){
		//"graphicsfile","physicsfile",{position},{lookat}
		popvector3d(L,&lx,&ly,&lz);
	}else{
		lx = 1; ly = 1; lz = 1;
	}

	if(nargs > 3){
		//"graphicsfile","physicsfile",{position}
		popvector3d(L,&x,&y,&z);
	}else{
		x = 0; y = 0; z = 0;
	}
	//"graphicsfile","physicsfile",mass

	double mass = lua_tonumber(L,-1);
	const char *ppath = lua_tostring(L,-2);
	const char *gpath = lua_tostring(L,-3);
	lua_pop(L,3);//

	ISceneManager *smgr = device->getSceneManager();
	
	//printf("bphysnode, creating the scene node\n");

	//Create the scene node
	//IMeshBuffer *buf = new CDynamicMeshBuffer(E_VERTEX_TYPE::EVT_STANDARD,E_INDEX_TYPE::EIT_16BIT);
	//IMeshBuffer *buf = new SMeshBuffer();
	//tinyobj_attrib_t attrib;
	//printf("At the very beginning, attrib is %p\n",(void*)&attrib);
	//tinyobj_shape_t *shapes = NULL;
	//size_t meshcount;
	//tinyobj_material_t *materials = NULL;
	//size_t num_materials;

	////Read data from the graphics file
	//size_t data_len = 0;
	//FILE *objfile = fopen(gpath,"rb");
	//fseek(objfile,0,SEEK_END);
	//data_len = ftell(objfile);
	//printf("model data is %d long\n",(int)data_len);
	//fseek(objfile,0,SEEK_SET);
	//char *objdata = (char*)malloc(sizeof(char)*data_len);
	//fread(objdata, sizeof(char), data_len, objfile);
	//fclose(objfile);

	////Parse the data into a tinyobj
	//printf("About to tinyobj_parse_obj\n");
	//int err = tinyobj_parse_obj(&attrib, &shapes, &meshcount, &materials, &num_materials, objdata, data_len, TINYOBJ_FLAG_TRIANGULATE);
	//free(objdata);

	////int err = tinyobj_parse_obj(&attrib, &shapes, &meshcount, &materials, &num_materials, objdata, data_len, 0);
	//if(err != TINYOBJ_SUCCESS){
		//printf("Tinyobj failed to load model:%s\n",ppath);
	//}
	//printf("num_shapes: %d\n",(int)meshcount);
	//for(size_t i = 0; i < meshcount-1; i++){// 0 - meshcount, so -1
		//tinyobj_shape_t shape = shapes[i];
		//printf("Shape %d:\n\tname: %s\n\tface_offset: %d\n\tlength: %d\n",(int)i, shape.name, shape.face_offset, shape.length);
	//}
	////u32 meshcount = pmesh->getMeshBufferCount();
	////size_t numverts = attrib.num_vertices;
	////size_t stride = 9; //9 = 3 position floats + 3 normal floats + 3 color floats
	////size_t face_offset = 0;
	//printf("Obj attrib:\n\tnum_vertices:%d\n\tnum_normals:%d\n\tnum_texcoords:%d\n\tnum_faces:%d\n\tnum_face_num_verts:%d\n",
			//attrib.num_vertices, attrib.num_normals, attrib.num_texcoords, attrib.num_faces, attrib.num_face_num_verts);
	////S3DVertex verts[attrib.num_faces];
	////for(size_t i = 0; i < attrib.num_face_num_verts; i++){
		////printf("verts %d starts at %p\n",(int)i,(void*)&(verts[i]));
	////}
	////u16 indicies[attrib.num_faces];
	//size_t index_c = 0;
	//printf("Faces for %d verts:\n",attrib.num_face_num_verts);
	////int num_faces = attrib.num_faces;
	////for(int i = 0; i < num_faces; i+=3){
		//////printf("\t%u verts in face %u\n",attrib.face_num_verts[i],i);
		////tinyobj_vertex_index_t fac1 = attrib.faces[i + 0];
		////tinyobj_vertex_index_t fac2 = attrib.faces[i + 1];
		////tinyobj_vertex_index_t fac3 = attrib.faces[i + 2];
		//////printf("Verts:\n\tPosition, normal, texture\n\t(%8d %7d %7d)\n\t(%8d %7d %7d)\n\t(%8d %7d %7d)\n",fac1.v_idx,fac1.vn_idx,fac1.vt_idx,fac2.v_idx,fac2.vn_idx,fac2.vt_idx,fac3.v_idx,fac3.vn_idx,fac3.vt_idx);

		////float f1px, f1py, f1pz;
		////f1px = attrib.vertices[(3 * fac1.v_idx) + 0];
		////f1py = attrib.vertices[(3 * fac1.v_idx) + 1];
		////f1pz = attrib.vertices[(3 * fac1.v_idx) + 2];
		////float f1nx, f1ny, f1nz;
		////f1nx = attrib.normals[(3 * fac1.vn_idx) + 0];
		////f1ny = attrib.normals[(3 * fac1.vn_idx) + 1];
		////f1nz = attrib.normals[(3 * fac1.vn_idx) + 2];

		////float f2px, f2py, f2pz;
		////f2px = attrib.vertices[(3 * fac2.v_idx) + 0];
		////f2py = attrib.vertices[(3 * fac2.v_idx) + 1];
		////f2pz = attrib.vertices[(3 * fac2.v_idx) + 2];
		////float f2nx, f2ny, f2nz;
		////f2nx = attrib.normals[(3 * fac2.vn_idx) + 0];
		////f2ny = attrib.normals[(3 * fac2.vn_idx) + 1];
		////f2nz = attrib.normals[(3 * fac2.vn_idx) + 2];

		////float f3px, f3py, f3pz;
		////f3px = attrib.vertices[(3 * fac3.v_idx) + 0];
		////f3py = attrib.vertices[(3 * fac3.v_idx) + 1];
		////f3pz = attrib.vertices[(3 * fac3.v_idx) + 2];
		////float f3nx, f3ny, f3nz;
		////f3nx = attrib.normals[(3 * fac3.vn_idx) + 0];
		////f3ny = attrib.normals[(3 * fac3.vn_idx) + 1];
		////f3nz = attrib.normals[(3 * fac3.vn_idx) + 2];
		/*
		//printf("Triangle %d:\n\t\
	////Positions:\n\t\t\
		////(%f %f %f)\n\t\t\
		////(%f %f %f)\n\t\t\
		////(%f %f %f)\n\t\
	////Normals:\n\t\t\
		////(%f %f %f)\n\t\t\
		////(%f %f %f)\n\t\t\
		////(%f %f %f)\n",((int)i)/3,f1px,f1py,f1pz,f2px,f2py,f2pz,f3px,f3py,f3pz,f1nx,f1ny,f1nz,f2nx,f2ny,f2nz,f3nx,f3ny,f3nz);
		*/
		//////float 

		////verts[i + 0] = S3DVertex(f1px,f1py,f1pz,f1nx,f1ny,f1nz,SColor(255,255,255,255),0,0);
		////verts[i + 1] = S3DVertex(f2px,f2py,f2pz,f2nx,f2ny,f2nz,SColor(255,255,255,255),0,0);
		////verts[i + 2] = S3DVertex(f3px,f3py,f3pz,f3nx,f3ny,f3nz,SColor(255,255,255,255),0,0);
		////indicies[index_c] = i;
		////index_c++;
		////indicies[index_c] = i + 1;
		////index_c++;
		////indicies[index_c] = i + 2;
		////index_c++;
	////}
	//S3DVertex verts[attrib.num_vertices];
	////S3DVertex real_verts[attrib.num_faces];
	//u16 indicies[attrib.num_faces];
	//for(size_t i = 0; i < attrib.num_vertices - 1; i++){ //0 - num_vertices, so -1
		//float x,y,z;
		//x = attrib.vertices[(i * 3) + 0];
		//y = attrib.vertices[(i * 3) + 1];
		//z = attrib.vertices[(i * 3) + 2];
		//__mingw_printf("Vertex number %llu: (%3f, %3f, %3f)\n",i,x,y,z);
		//verts[i] = S3DVertex(x,y,z,0,0,0,SColor(255,255,255,255),0,0);
	//}
	//for(size_t i = 0; i < attrib.num_face_vertices; i++){

	//}
	//for(size_t i = 0; i < attrib.num_faces - 1; i ++){// 0 - num_faces, so -1
		//__mingw_printf("Found index: %llu: %3d\n",i,attrib.faces[i].v_idx);
		////real_verts[i]
		//indicies[i] = attrib.faces[i].v_idx;
	//}
	//printf("indicies:\n");
	//for(size_t i = 0; i < index_c; i++){
		//printf("%d ",indicies[i]);
	//}
	//printf("\n");

	////for(size_t i = 0; i < attrib.num_faces; i++){
		////tinyobj_vertex_index_t tface = attrib.faces[i];
		////printf("Looking at face %d: %d %d %d\n", (int)i, (int)tface.v_idx, (int)tface.vt_idx, (int)tface.vn_idx);
		////indicies[index_c] = (u16)tface.v_idx;
		////index_c++;
	////}
	//buf->append(verts, attrib.num_vertices, indicies, (u32)attrib.num_faces);
	////buf->recalculateBoundingBox();
	//SMesh *gmesh = new SMesh();
	//gmesh->addMeshBuffer(buf);
	////gmesh->recalculateBoundingBox();
	////gmesh->setDirty();

	IMesh *gmesh = smgr->getMesh(gpath);
	//printf("Creating client physbox at %f %f %f\n",x,y,z);
	ISceneNode *node = smgr->addMeshSceneNode(gmesh,0,-1,vector3df(x,y,z));

	//printf("bphysnode, createing the physics body\n");
	//Create the physics body
	lua_pushstring(L,ppath);//"physpath"
	lua_pushnumber(L,mass);//"physpath",mass
	pushvector3d(L,x,y,z);//"physpath",mass,{x,y,z}
	pushvector3d(L,lx,ly,lz);//"physpath,mass,{x,y,z},{lx,ly,lz}
	//printf("About to makebphysmodel\n");
	makebphysmodel(L);//ud_rigidbody
	//printf("done makebphysmodel\n");

	btRigidBody *rb = (btRigidBody*)lua_touserdata(L,-1);
	rb->setUserPointer(node);
	lua_pop(L,1);

	//Create the lua representation
	lua_newtable(L);//{}

	lua_pushlightuserdata(L,rb);
	lua_setfield(L,-2,"collider");//{rb=ud_rb}

	lua_pushstring(L,"rigidbody");
	lua_setfield(L,-2,"type");//{rb=ud_rb, type="rigidbody"}

	lua_pushlightuserdata(L,node);
	lua_setfield(L,-2,"node");//{rb=ud_rb, node=ud_node, type="rigidbody"}

	luaL_getmetatable(L,"phys.physmodel");
	lua_setmetatable(L,-2);//{physnode}

	lua_getglobal(L,"phys");//{rb},{phys}
	lua_getfield(L,-1,"colliders");//{rb},{phys},{phys.colliders}
	lua_pushlightuserdata(L,rb);//{rb},{phys},{colliders},ud_rb
	lua_pushvalue(L,-4);//{rb},{phys},{colliders},ud_rb,{rb}
	lua_settable(L,-3);//{rb},{phys},{phys.colliders}
	lua_pop(L,2);//{rb}
	//printf("finished creating the lua representation\n");

	return 1;
}

static const luaL_reg bphysmodel_f[] = {
	{"newphysmodel",           newbphysmodel},
	{0,0},
};

static const luaL_reg bphysmodel_m[] = {
	{0, 0},
};

void cbphysmodel_register(lua_State* L){
	assert(lua_gettop(L) == 0);
	bphysmodel_register(L);
	assert(lua_gettop(L) == 0);

	assert(lua_gettop(L) == 0);
	luaL_getmetatable(L,"phys.physmodel");//{physmodel_m}
	lua_getfield(L,-1,"__index");//{physmodel_m},{}
	luaL_register(L,NULL,igeneric_m); //Inherit all the things to do with scene nodes
	luaL_register(L,NULL,bcollider_m);//Override things to do with physics
	luaL_register(L,NULL,brigidbody_m);//Add rigidbody things
	luaL_register(L,NULL,bphysmodel_m);//anything specific?
	lua_pop(L,2);
	assert(lua_gettop(L) == 0);
	
	assert(lua_gettop(L) == 0);
	lua_getglobal(L,"phys");
	luaL_register(L,NULL,bphysmodel_f);
	lua_pop(L,1);
	assert(lua_gettop(L) == 0);
}
