#include <stdio.h>
#include <stdlib.h>
#include <list>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

#include <btBulletDynamicsCommon.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <irrlicht.h>
#include "cbphysbox.hpp"
#include "../scene/imesh.hpp"
#include "../scene/igeneric.hpp"
#include <shared/lua_api/phys/bcharactercontroller.hpp>
#include <shared/lua_api/common.hpp>

#include <shared/lua_api/phys/bphysgeneric.hpp>

using namespace irr;
using namespace scene;
using namespace core;
using namespace video;

extern IrrlichtDevice* device;

extern btDiscreteDynamicsWorld* World;
extern std::list<btRigidBody*> Objects;


//phys.newcharactercontroller({vector3 size},{vector3 origin})
static int newcbcharactercontroller(lua_State* L){//
	printf("Createing new cbcharactercontroller\n");
	double sx,sy,sz,x,y,z;
	//Get the data
	popvector3d(L,&x,&y,&z);//{v3 size}
	popvector3d(L,&sx,&sy,&sz);//

	pushvector3d(L,sx,sy,sz);//{v3 size}
	pushvector3d(L,x,y,z);//{v3 size},{v3 origin}
	makenewbcharactercontroller(L);//ud_cc
	btKinematicCharacterController* cc = (btKinematicCharacterController*)lua_touserdata(L,-1);//ud_cc
	lua_pop(L,1);

	pushvector3d(L,sx,sy,sz);//{v3 size}
	pushvector3d(L,x,y,z);//{v3 size},{v3 origin}
	makenewiscenecube(L);//ud_iscenenode
	ISceneNode* n = (ISceneNode*)lua_touserdata(L,-1);//ud_iscenenode
	lua_pop(L,1);

	cc->getGhostObject()->setUserPointer(n);
	//cc->setUserPointer(n);//TODO: what does this break?

	lua_newtable(L);//{}
	lua_pushlightuserdata(L,cc);//{},ud_cc
	lua_setfield(L,-2,"character");//{}
	lua_pushlightuserdata(L,n);//{},ud_iscenenode
	lua_setfield(L,-2,"node");//{}
	lua_pushstring(L,"character");
	lua_setfield(L,-2,"type");

	luaL_getmetatable(L,"phys.charactercontroller");//{},{phys.charactercontroller}
	lua_setmetatable(L,-2);//{}

	return 1;
}

//setMaterial(self,material)
//int cbsetmaterial(lua_State* L){
	//printf("Call to setmaterial\n");
	////SMaterial* mat = (SMaterial*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_IMaterial
	//ITexture* tex = (ITexture*)lua_touserdata(L,-1);
	//lua_pop(L,1);//{node=ud_ISceneNode}
	//printf("About to get field node\n");
	//lua_getfield(L,-1,"node");//{node=ud_ISceneNode},ud_ISceneNode
	//printf("After call to field node\n");
	//ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	//lua_pop(L,2);//

	//lua_pushlightuserdata(L,i);
	//lua_pushlightuserdata(L,tex);
	//printf("Finished getting everything for setmaterial\n");
	//iscenesetmaterial(L);

	//return 0;
//}

//int cbchar

static const luaL_reg cbcharactercontroller_m[] = {
	//{"setpos",	cbcharsetpos},//overload
	//{"getpos",	cbchargetpos},
	//{"getgravity",  cbphysgetgravity},
	//{"applygravity",cbphysapplygravity},
	//{"setMaterial", cbsetmaterial},
//	{"delete",	delbphysbox},//client side delete needs to delete the visual representation
	{0, 0},
};

void cbcharactercontroller_register(lua_State* L){
	bcharactercontroller_register(L);//
	lua_getglobal(L,"phys");//{}
	lua_pushcfunction(L,newcbcharactercontroller);//{},newcbphysbox()
	lua_setfield(L,-2,"newccharactercontroller");//{}

	lua_pop(L,1);//

	luaL_getmetatable(L,"phys.charactercontroller");//phys.physbox
	lua_newtable(L);//phys.physbox,{}
	//luaL_register(L,NULL,brigidbody_m);
	luaL_register(L,NULL,igeneric_m);
	luaL_register(L,NULL,cbcharactercontroller_m);//phys.physbox,{}
	luaL_register(L,NULL,bcharactercontroller_m);
	lua_setfield(L,-2,"__index");//phys.physbox

	lua_pop(L,1);

	//printf("When registering physbox, new() is %p\n",newcbphysbox);
	//printf("setpos is %p\n",cbphyssetpos);

}
