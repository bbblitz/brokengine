
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "video/smaterial.hpp"
#include "video/itexture.hpp"
#include "video/iimage.hpp"
#include "video/draw.hpp"

#include <shared/lua_api/common.hpp>

using namespace irr;
using namespace video;
using namespace core;

extern IrrlichtDevice* device;
extern IVideoDriver* driver;

//video.drawtexture
//{texture},{x,y}
//{texture},{x,y},{sourcerect},{color},use_alpha
int draw2dimage(lua_State* L){
	int nargs = lua_gettop(L);
	//printf("Drawing a 2d image\n");
	if(nargs == 2){
		//printf("2-argument version\n");
		long x,y;
		popvector2i(L,&x,&y);
		lua_getfield(L,-1,"texture");
		ITexture *tex = (ITexture*)lua_touserdata(L,-1);
		lua_pop(L,2);
		driver->draw2DImage(tex,position2d<s32>(x,y));
	}else if(nargs == 5){
		//printf("5-argument version\n");
		int usealpha = lua_toboolean(L,-1);
		lua_pop(L,1);
		//printf("Got usealpha: %d\n",usealpha);
		long r,g,b,a;
		popvector4i(L,&r,&g,&b,&a);
		long ssx,ssy,sex,sey;
		poprecti(L,&ssx,&ssy,&sex,&sey);
		long x,y;
		popvector2i(L,&x,&y);
		lua_getfield(L,-1,"texture");
		ITexture *tex = (ITexture*)lua_touserdata(L,-1);
		if(tex == NULL){
			lua_pushstring(L,"Tried to draw a NULL texture");
			lua_error(L);
		}
		lua_pop(L,2);
		rect<s32> clipedto;
		driver->draw2DImage(
				tex,
				position2d<s32>(x,y),
				rect<s32>(ssx,ssy,sex,sey),
				NULL,
				SColor(a,r,g,b),
				usealpha == 1
		);
	}else{
		lua_pushstring(L,"Incorrect number of arguments to video.drawtexture() (expected 2 or 6)");
		lua_error(L);
	}
	return 0;
}

//{sx,sy},{ex,ey},{color}
int draw2dline(lua_State* L){
	long sx,sy,ex,ey;
	long r,g,b,a;
	popvector4i(L,&r,&g,&b,&a);
	popvector2i(L,&ex,&ey);
	popvector2i(L,&sx,&sy);

	driver->draw2DLine(position2d<s32>(sx,sy),position2d<s32>(ex,ey),SColor(a,r,g,b));

	return 0;
}

//{sx,sy,sz},{ex,ey,ez},{color}
int draw3dline(lua_State* L){
	double sx,sy,sz;
	double ex,ey,ez;
	long r,g,b,a;
	popvector4i(L,&r,&g,&b,&a);
	popvector3d(L,&ex,&ey,&ez);
	popvector3d(L,&sx,&sy,&sz);

	driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);
	driver->draw3DLine(vector3df(sx,sy,sz),vector3df(ex,ey,ez),SColor(a,r,g,b));

	return 0;
}

void load_videofuncs(lua_State* L){
	//printf("Loading video libraries...\n");
	lua_newtable(L);//{}
	lua_setglobal(L,"video");//

	lua_getglobal(L,"video");//{}
	lua_pushcfunction(L,draw2dimage);//{},draw2dimage()
	lua_setfield(L,-2,"drawtexture");//{}
	lua_pushcfunction(L,draw2dline);//{},draw2dline()
	lua_setfield(L,-2,"draw2dline");//{}
	lua_pushcfunction(L,draw3dline);
	lua_setfield(L,-2,"draw3dline");
	lua_pop(L,1);//

	smaterial_register(L);
	itexture_register(L);
	iimage_register(L);
	draw_register(L);
	
}
