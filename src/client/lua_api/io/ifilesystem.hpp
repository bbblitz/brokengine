#ifndef __H_ifilesystem
#define __H_ifilesystem

extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void ifilesystem_register(lua_State* L);

#endif
