#include "ifilesystem.hpp"

/***
@module io
*/
using namespace irr;
using namespace io;

extern IrrlichtDevice* device;


/***
A list of files in the current direcotry.
@function list()
@treturn array The files and directories in the current directory.
*/
// io.list()
int listfilesin(lua_State *L){
	IFileSystem *fs = device->getFileSystem();
	IFileList *fl = fs->createFileList();

	unsigned long fc = fl->getFileCount();
	unsigned long i;
	const char *path;

	lua_newtable(L);
	for(i = 0; i < fc; i++){
		path = fl->getFileName(i).c_str();
		lua_pushnumber(L,i + 1);
		lua_pushstring(L,path);
		lua_settable(L,-3);
	}
	
	return 1;
}

/***
Changes the current directory of the program
@function cd(dir)
@tparam string dir The directory to change to
*/
// io.cd("directory")
int changedirectory(lua_State *L){
	IFileSystem *fs = device->getFileSystem();
	const char *dir = lua_tostring(L,-1);
	lua_pop(L,1);
	fs->changeWorkingDirectoryTo(path(dir));
	return 0;
}

/***
Logs some text with Irrlicht.
`level` may be any of:
io.LOG_DEBUG, io.LOG_INFO, io.LOG_WARN, io.LOG_ERROR, io.LOG_NONE
@function log(text,level[,hint])
@tparam string text The text to log
@tparam log_level_enum level The log level
@tparam string hint An optional hint to supply with the log
*/
// io.log("text",level)
// io.log("text",level[,"hint"])
int logmessage(lua_State *L){
	ILogger *log = device->getLogger();

	const char *hint_c = "";
	const char *text_c = "";
	int nargs = lua_gettop(L);
	if(nargs > 2){
		hint_c = lua_tostring(L,-1);
		lua_pop(L,1);//"text",level
	}
	ELOG_LEVEL ll = (ELOG_LEVEL)lua_tointeger(L,-1);//"text",level
	lua_pop(L,1);//"text"
	text_c = lua_tostring(L,-1);//"text"
	lua_pop(L,1);//
	
	log->log(text_c,hint_c,ll);

	return 0;
}

/***
Sets what output gets logged, and what gets ignored.
level may be any of:
io.LOG_DEBUG, io.LOG_INFO, io.LOG_WARN, io.LOG_ERROR, io.LOG_NONE
@function set_log_level(level)
@tparam number level the minimul level of log to capture
*/
//io.set_log_level(level)
int setloglevel(lua_State *L){
	ILogger *log = device->getLogger();
	
	ELOG_LEVEL ll = (ELOG_LEVEL)lua_tointeger(L,-1);
	log->setLogLevel(ll);

	return 0;
}

static const luaL_reg ifilesystem_f[] = {
	{"log",             logmessage},
	{"set_log_level",   setloglevel},
	{"list",            listfilesin},
	{"cd",          changedirectory},
	{0,0},
};

static const luaL_reg ifilesystem_m[] = {
	{0,0},
};

void ifilesystem_register(lua_State* L){

	luaL_newmetatable(L, "io.ifilesystem");//{m_iguibutton}
	lua_newtable(L);//{m_iguibutton},{}
	luaL_register(L,NULL,ifilesystem_m);
	lua_setfield(L,-2,"__index");//{m_iguibutton}

	lua_pop(L,1);
	printf("set io global\n");
	lua_getglobal(L,"io");//{io}
	luaL_register(L,NULL,ifilesystem_f);

	lua_pushnumber(L,ELL_DEBUG);
	lua_setfield(L,-2,"LOG_DEBUG");
	lua_pushnumber(L,ELL_INFORMATION);
	lua_setfield(L,-2,"LOG_INFO");
	lua_pushnumber(L,ELL_WARNING);
	lua_setfield(L,-2,"LOG_WARN");
	lua_pushnumber(L,ELL_ERROR);
	lua_setfield(L,-2,"LOG_ERROR");
	lua_pushnumber(L,ELL_NONE);
	lua_setfield(L,-2,"LOG_NONE");
	
	lua_pop(L,1);//
}
