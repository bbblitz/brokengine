#include "../util/hashmap.h"
#include <btBulletDynamicsCommon.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

typedef struct LISceneNode {
    irr::scene::ISceneNode* n;
    map_t funcmap;
    const char* type;
} LISceneNode;

typedef struct LBPhysNode : LISceneNode {
    btCollisionObject* r;
} LBPhysNode;

extern lua_State* tL;
extern irr::IrrlichtDevice* gamedevice;
extern long nodenum;
