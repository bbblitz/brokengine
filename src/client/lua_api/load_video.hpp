#ifndef __H_loadvideo
#define __H_loadvideo
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void load_videofuncs(lua_State* L);
#endif
