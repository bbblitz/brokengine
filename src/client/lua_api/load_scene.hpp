#ifndef __H_loadscene
#define __H_loadscene
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void load_scenefuncs(lua_State* L);
#endif
