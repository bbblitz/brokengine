
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "smaterial.hpp"

using namespace irr::video;

/*This probably needs a _gc metamethod*/
//newsmaterial()
int newsmaterial(lua_State* L){

	SMaterial* mat = new SMaterial();
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,mat);//{},ud_smaterial
	lua_setfield(L,-2,"smaterial");//{smaterial}

	luaL_getmetatable(L,"video.smaterial");//{smaterial},{m_smaterial}
	lua_setmetatable(L,-2);//{smaterial}

	return 1;
}

//setTexture(self,int_num,{ITexture texture})
int setTexture(lua_State* L){
	lua_getfield(L,-1,"texture");
	ITexture* tex = (ITexture*)lua_touserdata(L,-1);
	lua_pop(L,2);
	double num = lua_tonumber(L,-1);
	lua_pop(L,1);
	lua_getfield(L,-1,"smaterial");
	SMaterial* self = (SMaterial*)lua_touserdata(L,-1);
	lua_pop(L,2);
	self->setTexture(num,tex);
	return 0;
}

//{Material},flag,state
int setFlag(lua_State* L){
	int b = lua_toboolean(L,-1);//{material},flag,state
	lua_pop(L,1);//{material},flag
	int flag = lua_tonumber(L,-1);//{material},flag
	lua_pop(L,1);//{material}
	lua_getfield(L,-1,"smaterial");//{material},ud_material
	SMaterial* mat = (SMaterial*)lua_touserdata(L,-1);
	lua_pop(L,2);
	mat->setFlag((E_MATERIAL_FLAG)flag,b == 1 ? true : false);
	return 0;
}

static const luaL_reg smaterial_m[] = {
  {"setTexture",        setTexture},
  {"setFlag",           setFlag},
  {0,0},
};

#define set_const(l,x) lua_pushstring(l,#x);lua_pushinteger(l,x);lua_settable(l,-3);

void smaterial_register(lua_State* L){
	//Add globals dealing with material flags

	luaL_newmetatable(L,"video.smaterial");//{m_smaterial}
	
	lua_newtable(L);//{m_smaterial},{}
	luaL_register(L,NULL,smaterial_m);//{m_smaterial},{smaterial}

	lua_setfield(L,-2,"__index");//{m_smaterial}

	lua_pop(L,1);//

	lua_getglobal(L,"video");//{}

	set_const(L,EMF_WIREFRAME);
	set_const(L,EMF_POINTCLOUD);
	set_const(L,EMF_GOURAUD_SHADING);
	set_const(L,EMF_LIGHTING);
	set_const(L,EMF_ZBUFFER);
	set_const(L,EMF_ZWRITE_ENABLE);
	set_const(L,EMF_BACK_FACE_CULLING);
	set_const(L,EMF_FRONT_FACE_CULLING);
	set_const(L,EMF_BILINEAR_FILTER);
	set_const(L,EMF_TRILINEAR_FILTER);
	set_const(L,EMF_ANISOTROPIC_FILTER);
	set_const(L,EMF_FOG_ENABLE);
	set_const(L,EMF_NORMALIZE_NORMALS);
	set_const(L,EMF_TEXTURE_WRAP);
	set_const(L,EMF_ANTI_ALIASING);
	set_const(L,EMF_COLOR_MASK);
	set_const(L,EMF_COLOR_MATERIAL);
	set_const(L,EMF_USE_MIP_MAPS);
	set_const(L,EMF_BLEND_OPERATION);
	set_const(L,EMF_POLYGON_OFFSET);

	lua_pushcfunction(L,newsmaterial);//{},newsmaterial
	lua_setfield(L,-2,"newsmaterial");//{}

	lua_pop(L,1);//

}
