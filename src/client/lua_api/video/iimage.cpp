
#include "iimage.hpp"
#include <irrlicht.h>

#include <shared/lua_api/common.hpp>

/***
@module video
*/

using namespace irr;
using namespace video;

extern IrrlichtDevice* device;
extern IVideoDriver* driver;

/***
Creates a new irrlicht image.
A newly created image will be fully white by default.
@function newiimage(format, size)
@tparam enum_color_format format The format for the image
@tparam vec2d size The size of the image
@treturn iimage The image
*/
//newiimage(ECOLOR_FORMAT,{width,height}) -> {image}
int newiimage(lua_State* L){
	//printf("Attempting to create new iimage\n");
	long w,h;
	popvector2i(L,&w,&h);
	int format = lua_tonumber(L,-1);
	lua_pop(L,1);
	//printf("All data collected, creating...\n");
	IImage* img = driver->createImage((irr::video::ECOLOR_FORMAT)format,irr::core::dimension2d<u32>(w,h));
	lua_newtable(L);
	lua_pushlightuserdata(L,img);
	lua_setfield(L,-2,"image");
	luaL_getmetatable(L,"iimage");
	lua_setmetatable(L,-2);
	//printf("Everything sets up, returning\n");
	return 1;
}

/***
Creates a new irrlicht image.
Creates a new irrlicht image from a file. The format and width/height will be
taken from the image file.
@function newiimagefromfile(path)
@tparam string path The file path of the image file
@treturn iimage The loaded image
*/
//newiimagefromfile("/path/to/file") -> {image}
int newiimagefromfile(lua_State* L){
	printf("Creating new iimage from file");
	const char* strpath = lua_tostring(L,-1);
	lua_pop(L,1);
	int numloaders = driver->getImageLoaderCount();
	bool hasloaded = false;
	IImage* img = NULL;
	printf("About to create and open file\n");
	io::IReadFile *f = device->getFileSystem()->createAndOpenFile(strpath);
	if(!f){
		lua_pushstring(L,"Failed to open file: ");
		lua_pushstring(L,strpath);
		lua_concat(L,2);
		lua_error(L);
	}
	printf("Opened file\n");
	for(int j = 0; j < numloaders; j++){
		IImageLoader* loader = driver->getImageLoader(j);
		if(!loader->isALoadableFileExtension(strpath))
			continue;
		if(!loader->isALoadableFileFormat(f))
			continue;
		f->seek(0,false);
		hasloaded = true;
		printf("Found a loader, about to load\n");
		img = loader->loadImage(f);
		printf("Done loading\n");
		f->drop();
	}
	if(!hasloaded){
		lua_pushstring(L,"Failed to load file, no image loader for this type.");
		lua_error(L);
	}
	if(!img){
		lua_pushstring(L,"Failed to load image");
		lua_error(L);
	}
	printf("Successfully loaded\n");
	lua_newtable(L);
	lua_pushlightuserdata(L,img);
	lua_setfield(L,-2,"image");
	luaL_getmetatable(L,"iimage");
	lua_setmetatable(L,-2);
	printf("IImage made, returning!");
	return 1;
}

/***
Sets the color of an image at a particular pixel.
@function iimage:setpixel(position,color,shouldblend)
@tparam vec2i position The position of the pixel to change colors
@tparam color color The color to change the pixel to
@tparam boolean shouldblend Should this pixel blend into it's neighbors?
*/
//setpixel({self},{x,y},{r,g,b,a},bool_shouldblend)
int setiimagepixel(lua_State* L){
	bool sb = lua_toboolean(L,-1);//{ud_iimage},{x,y},{r,g,b,a},bool_shouldblend
	lua_pop(L,1);//{ud_iimage},{x,y},{r,g,b,a}
	double r,g,b,a;
	popvector4d(L,&r,&g,&b,&a);//{ud_iimage},{x,y}
	long x,y;
	popvector2i(L,&x,&y);//{ud_iimage}
	lua_getfield(L,-1,"image");
	IImage* img = (IImage*)lua_touserdata(L,-1);//{ud_iimage}
	img->setPixel(x,y,SColor(a,r,g,b),sb);
	lua_pop(L,2);
	return 0;
}

/***
Returns the color at a position in the image.
@function iimage:getpixel(position)
@tparam vec2i position The position of the pixel to return
@treturn color The color of the image at the position
*/
//getpixel({self},{x,y})
int getiimagepixel(lua_State* L){
	long x,y;
	popvector2i(L,&x,&y);
	lua_getfield(L,-1,"image");
	IImage* img = (IImage*)lua_touserdata(L,-1);
	SColor color = img->getPixel(x,y);
	pushvector4i(L,color.getRed(), color.getBlue(), color.getGreen(), color.getAlpha());
	return 1;
}

/***
Returns the dimensions of the image
@function iimage:getDimensions()
@treturn vec2i dimensions The dimensions of the image
*/
//getdimensions({self})
int getiimagedimensions(lua_State *L){
	lua_getfield(L,-1,"image");
	IImage *img = (IImage*)lua_touserdata(L,-1);
	core::dimension2d<u32> dims = img->getDimension();
	pushvector2i(L,dims.Height, dims.Width);
	return 1;
}

static const luaL_reg iimage_m[] = {
  {"setpixel",        setiimagepixel},
  {"getpixel",        getiimagepixel},
  {"getdimensions",   getiimagedimensions},
  {0,0},
};

#define set_const(l,x) lua_pushstring(l,#x);lua_pushinteger(l,x);lua_settable(l,-3);

void iimage_register(lua_State* L){
	driver = device->getVideoDriver();
	lua_getglobal(L,"video");//{}

	set_const(L,ECF_A1R5G5B5);
	set_const(L,ECF_R5G6B5);
	set_const(L,ECF_A8R8G8B8);
	set_const(L,ECF_R16F);
	set_const(L,ECF_G16R16F);
	set_const(L,ECF_A16B16G16R16F);
	set_const(L,ECF_R32F);
	set_const(L,ECF_G32R32F);
	set_const(L,ECF_A32B32G32R32F);
	set_const(L,ECF_UNKNOWN);

	lua_pushcfunction(L,newiimage);//{},newiimage()
	lua_setfield(L,-2,"newiimage");//{}
	lua_pushcfunction(L,newiimagefromfile);//{},newiimagefromfile()
	lua_setfield(L,-2,"newiimagefromfile");//{}
	lua_pop(L,1);//

	luaL_newmetatable(L,"iimage");//{m_iimage}
	
	lua_newtable(L);//{m_iimage},{}
	luaL_register(L,NULL,iimage_m);//{m_iimage},{iimage}

	lua_setfield(L,-2,"__index");//{m_iimage}

	lua_pop(L,1);//
	
}
