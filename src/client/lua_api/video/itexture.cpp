
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "itexture.hpp"
#include "iimage.hpp"
#include <shared/lua_api/common.hpp>

using namespace irr;
using namespace video;

extern IrrlichtDevice* device;
extern IVideoDriver* driver;
//newtexture(string name,{image}) -> {texture}
int newitexture(lua_State* L){
	//printf("About to create new texture\n");
	lua_getfield(L,-1,"image");//"name",{image},image*
	IImage* im = (IImage*) lua_touserdata(L,-1);
	lua_pop(L,2);//"name"
	const char* name = lua_tostring(L,-1);
	lua_pop(L,1);//

	//printf("About to create texture\n");
	ITexture* tex = driver->addTexture(name,im);
	if(!tex){
		lua_pushstring(L,"Failed to create texture!");
		lua_error(L);
	}

	lua_newtable(L);
	lua_pushlightuserdata(L,tex);
	lua_setfield(L,-2,"texture");

	return 1;
}

//newtexturefromfile(string file_name) -> {texture}
int newitexturefromfile(lua_State* L){
	printf("New texture from file");
	const char* strpath = lua_tostring(L,-1);
	lua_pop(L,1);
	ITexture* tex = driver->getTexture(strpath);
	if(!tex){
		lua_pushstring(L,"Failed to create texture!");
		lua_error(L);
	}

	lua_newtable(L);
	lua_pushlightuserdata(L,tex);
	lua_setfield(L,-2,"texture");

	return 1;
}



void itexture_register(lua_State* L){
	lua_getglobal(L,"video");//{}
	lua_pushcfunction(L,newitexture);//{},newitexture
	lua_setfield(L,-2,"newtexture");//{}
	lua_pushcfunction(L,newitexturefromfile);
	lua_setfield(L,-2,"newtexturefromfile");
	lua_pop(L,1);
}
