
#include <irrlicht.h>
#include <shared/lua_api/common.hpp>
#include "draw.hpp"

using namespace irr;
using namespace video;
using namespace core;

extern IrrlichtDevice* device;
extern IVideoDriver* driver;
//drawline2d {startx,starty},{endx,endy},{r,g,b,a}
int drawline2d(lua_State *L){
	long r,g,b,a;
	popvector4i(L, &r, &g, &b, &a);
	long endx, endy;
	popvector2i(L, &endx, &endy);
	long startx, starty;
	popvector2i(L, &startx, &starty);
	driver->draw2DLine(position2d<s32>(startx, starty), position2d<s32>(endx,endy), SColor(r,g,b,a));
	return 0;
}

void draw_register(lua_State *L){
	lua_getglobal(L,"video");//{video}
	lua_pushcfunction(L,drawline2d);//{video},drawline2d()
	lua_setfield(L,-2,"drawline2d");//{video}
	lua_pop(L,1);//
}
