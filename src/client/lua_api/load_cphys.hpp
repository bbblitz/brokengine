#ifndef __H_loadphys
#define __H_loadphys
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void load_cphysfuncs(lua_State* L);
#endif
