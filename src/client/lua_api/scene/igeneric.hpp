#ifndef __H_iscenegeneric
#define __H_iscenegeneric
#include <stdio.h>
#include <stdlib.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

int iscenegetpos(lua_State* L);
int iscenesetpos(lua_State* L);
int iscenegetangle(lua_State* L);
int iscenesetangle(lua_State* L);
int iscenesetmaterial(lua_State* L);

extern const luaL_reg igeneric_m[];
#endif
