/*This file defines some things that all igui stuff can do*/
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "igeneric.hpp"
#include "../gameparts.hpp"
#include <shared/lua_api/common.hpp>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;

/***
@classmod iscenenode 
*/

extern IrrlichtDevice* device;



/***
Get the position of a scene element.
@function iscenenode:getpos()
@treturn vector3d position The position this element is at
*/
int iscenegetpos(lua_State* L){//{node=ud_IMeshSceneNode}
	lua_getfield(L,-1,"node");//{node=ud_IMeshSceneNode},ud_IMeshSceneNode
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_IMeshSceneNode},ud_IMeshSceneNode
	vector3df pos = i->getAbsolutePosition();
	lua_pop(L,2);
	pushvector3d(L,pos.X,pos.Y,pos.Z);
	
	return 1;
}

/***
Set the position of a scene element.
@function iscenenode:setpos(vector3d)
@tparam vector3d position The position to set this element to
*/
int iscenesetpos(lua_State* L){//{node=ISceneNode},{x,y,z}
	double x,y,z;
	popvector3d(L,&x,&y,&z);//{node=ud_ISceneNode}
	lua_getfield(L,-1,"node");//{node=ud_ISceneNode},ud_ISceneNode
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	i->setPosition(vector3df(x,y,z));
	i->updateAbsolutePosition();
	lua_pop(L,2);//
	return 0;
}

/***
Set the rotation of a scene element.
@function iscenenode:getang()
@treturn angle3d The angle of this element
*/
int iscenegetangle(lua_State* L){//{node=ud-IMeshSceneNode}
	lua_getfield(L,-1,"node");
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);
	irr::core::vector3df ang = i->getRotation();
	pushvector3d(L,ang.X, ang.Y, ang.Z);
	return 1;
}

/***
Set the rotation of a scene element.
@function iscenenode:getang()
@tparam angle3d angle The angle to set this element to
*/
int iscenesetangle(lua_State* L){//{node=ud_ISceneNode},{x,y,z}
	double x,y,z;
	popvector3d(L,&x,&y,&z);
	lua_getfield(L,-1,"node");
	ISceneNode* i = (ISceneNode*)lua_touserdata(L,-1);
	irr::core::vector3df ang = irr::core::vector3df(x,y,z);
	i->setRotation(ang);
	return 0;
}

/***
Set the mateiral of a scene element.
@function iscenenode:setmaterial()
@tparam texture texture The texture to apply to this material
*/
//iscenesetmaterialtexture({node=ud_ISceneNode},{texture=ud_itexture},texture_number=0)
int iscenesetmaterial(lua_State *L){
	printf("Calling generic iscenesetmaterial function\n");
	int args = lua_gettop(L);
	u32 layer = 0;
	if(args == 3){
		layer = lua_tonumber(L,-1);
		lua_pop(L,1);
	}
	lua_getfield(L,-1,"texture");
	ITexture *txt = (ITexture*)lua_touserdata(L,-1);//{node=ud_ISceneNode},{texture=ud_itexture},ud_itexture
	lua_pop(L,2);//{node=ud_ISceneNode}
	lua_getfield(L,-1,"node");
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);//

	i->setMaterialTexture(layer,txt);
    
	return 0;
}

/***
Set the name of a scene element.
@function iscenenode:setname()
@tparam string name The name to give this scene element.
*/
//iscenesetname({node=ud_ISceneNode},str_name)
int iscenesetname(lua_State *L){
	const char *name = lua_tostring(L,-1);
	lua_pop(L,1);//{node=ud_ISceneNode}

	lua_getfield(L,-1,"node");//{node=ud_ISceneNode},ud_ISceneNode
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);

	i->setName((c8*)name);
	return 0;
}

/***
Get the name of this scene element.
@function iscenenode:getname()
@tparam string name The name to give this scene element.
*/
//iscenesetname({node=ud_ISceneNode})
int iscenegetname(lua_State *L){
	lua_getfield(L,-1,"node");//{node=ud_ISceneNode},ud_ISceneNode
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);

	const char *name = i->getName();

	lua_pushstring(L,name);
	return 1;
}

/***
Set the scale of this scene element.
@function iscenenode:setscale({x,y,z})
@tparam vector3d scale The scale to give this scene element.
*/
//iscenesetscale({x,y,z})
int iscenesetscale(lua_State *L){
	double x,y,z;
	popvector3d(L,&x,&y,&z);

	lua_getfield(L,-1,"node");
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);

	i->setScale(vector3df(x,y,z));
	return 1;
}

/***
Sets the visibility of this scene element
@function iscenenode:setvisible(bool)
@tparam boolean visible Sets the visibility for this element
*/
//setvisible(true|false)
int iscenesetvisible(lua_State *L){
	int visible = lua_toboolean(L,-1);
	lua_pop(L,1);
	lua_getfield(L, -1, "node");
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode}, ud_ISceneNode
	lua_pop(L,2);

	i->setVisible(visible == 1);
	return 0;
}

/***
Get the scale of this scene element.
@function iscenenode:getscale()
@treturn vector3d The scale scale of this element.
*/
//iscenegetscale()
int iscenegetscale(lua_State *L){
	lua_getfield(L,-1,"node");
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);

	vector3df scale = i->getScale();
	double x,y,z;
	x = scale.X;
	y = scale.Y;
	z = scale.Z;

	pushvector3d(L,x,y,z);
	return 1;
}

/***
Get the scale of this scene element.
@function iscenenode:getscale()
@treturn vector3d The scale scale of this element.
*/
//iscenenode:remove()
int isceneremove(lua_State *L){
	lua_getfield(L,-1,"node");
	ISceneNode *i = (ISceneNode*)lua_touserdata(L,-1);//{node=ud_ISceneNode},ud_ISceneNode
	lua_pop(L,2);
	
	i->remove();

	return 1;
}

extern const luaL_reg igeneric_m[] = {
	{"getpos",        iscenegetpos},
	{"setpos",        iscenesetpos},
	{"getang",        iscenegetangle},
	{"setang",        iscenesetangle},
	{"setmaterialtexture",   iscenesetmaterial},
	{"getname",       iscenegetname},
	{"setname",       iscenesetname},
	{"setscale",      iscenesetscale},
	{"getscale",      iscenegetscale},
	{"remove",        isceneremove},
	{"setvisible",    iscenesetvisible},
	{0, 0},
};
