
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../gameparts.hpp"
#include "ilight.hpp"
#include "igeneric.hpp"
#include <shared/lua_api/common.hpp>

using namespace irr;
using namespace video;
using namespace scene;
using namespace core;

extern IrrlichtDevice* device;

//{} :: scene.newlight(radius, {v3 position})
static int newiscenelight(lua_State* L){
	printf("Createing light!\n");
	double x,y,z;
	popvector3d(L,&x,&y,&z);//radius
	double radius = lua_tonumber(L,-1);
    	lua_pop(L,1);//

	//Create the mesh
	ISceneManager* smgr = device->getSceneManager();
	ILightSceneNode* light = smgr->addLightSceneNode(0,vector3df(x,y,z),video::SColor(1.0f,1.0f,1.0f,1.0f),(f32)radius,(s32)-1);

	//Create it's lua representation
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,light);
	lua_setfield(L,-2,"node");

	//Set it's metatable
	luaL_getmetatable(L, "scene.imesh");
	lua_setmetatable(L, -2);

	return 1;

}

//settype(self,const)
int settype(lua_State *L){
	video::E_LIGHT_TYPE type = (video::E_LIGHT_TYPE)lua_tonumber(L,-1);//self,type
	lua_pop(L,1);//self
	lua_getfield(L,-1,"node");//self,ud_ILightSceneNode
	ILightSceneNode *light = (ILightSceneNode*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	light->setLightType(type);
	return 0;
}

static const luaL_reg ilight_m[] = {
	{"getpos",        iscenegetpos},
	{"setpos",        iscenesetpos},
	{"settype",       settype},
	{0, 0},
};

void ilight_register(lua_State* L){

	lua_getglobal(L,"scene");//{scene}
	lua_pushcfunction(L,newiscenelight);//{scene},newiscenelight()
	lua_setfield(L,-2,"newlight");//{scene}

	set_const(L,ELT_POINT);
	set_const(L,ELT_SPOT);
	set_const(L,ELT_DIRECTIONAL);

	lua_pop(L,1);//

	luaL_newmetatable(L,"scene.ilight");//scene.ilight
	lua_newtable(L);//scene.ilight,{}
	luaL_register(L, NULL, ilight_m);//scene.ilight,{}
	lua_setfield(L,-2,"__index");//scene.ilight

	lua_pop(L,1);
}
