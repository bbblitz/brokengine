
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../gameparts.hpp"
#include "imesh.hpp"
#include "igeneric.hpp"
#include <shared/lua_api/common.hpp>

/***
@module scene
*/
using namespace irr;
using namespace scene;
using namespace core;
using namespace video;

extern IrrlichtDevice* device;

//{} :: scene.newmesh("/path/to/model")
static int newiscenemesh(lua_State* L){//"path/to"

	printf("Createing mesh!\n");
	int nargs = lua_gettop(L);
	if(nargs != 1){
		lua_pushfstring(L,"scene.newmesh got %d arguments, expecting 1",nargs);
		lua_error(L);
	}
	//The model for the mesh
	const char* modelpath = luaL_optstring(L,1,"error");//"path/to"
	lua_pop(L,1);//

	//Create the mesh
	ISceneManager* smgr = device->getSceneManager();
	IAnimatedMesh* amesh = smgr->getMesh(modelpath);
	IAnimatedMeshSceneNode* mesh = smgr->addAnimatedMeshSceneNode( amesh );
	mesh->setMaterialFlag(EMF_GOURAUD_SHADING,true);
	printf("Registered the mesh!\n");

	lua_newtable(L);//{}
	luaL_getmetatable(L,"scene.imesh");//{},scene.imesh
	lua_setmetatable(L,-2);//{}

	lua_pushlightuserdata(L,mesh);//{},ud_mesh
	lua_setfield(L,-2,"node");//{}

	return 1;
}

// ud_node :: ({v3 size}, {v3 origin})
void makenewiscenecube(lua_State* L){
	double x,y,z;
	popvector3d(L,&x,&y,&z);//{v3 size}
	double sx,sy,sz;
	popvector3d(L,&sx,&sy,&sz);//{}
	printf("Makeing new iscenecube, size %f %f %f, origin %f %f %f\n", sx,sy,sz,x,y,z);
	ISceneNode* n = device->getSceneManager()->addCubeSceneNode(1,0,-1,core::vector3df(x,y,z),core::vector3df(0,0,0),core::vector3df(sx,sy,sz));
	lua_pushlightuserdata(L,n);
}

// {} :: scene.newcube({v3 size}, {v3 origin})
int newiscenecube(lua_State* L){//{v3 size}, {v3 origin}
	makenewiscenecube(L);//ud_node
	ISceneNode* n = (ISceneNode*)lua_touserdata(L,-1);//ud_node
	lua_pop(L,1);//
	lua_newtable(L);//{}
	luaL_getmetatable(L,"scene.icube");//{},scene.icube
	lua_setmetatable(L,-2);//{}

	lua_pushlightuserdata(L,n);//{},ud_mesh
	lua_setfield(L,-2,"node");//{}

	return 1;
}

static const luaL_reg imesh_m[] = {
	//{"setMaterial",          iscenesetmaterial},
	//{"getpos",               iscenegetpos},
	//{"setpos",               iscenesetpos},
	//  {"remove",        removeiguielement},
	{0, 0},
};

static const luaL_reg icube_m[] = {
	{0,0},
};

void imesh_register(lua_State* L){

	luaL_newmetatable(L, "scene.imesh");//scene.icamera
	lua_newtable(L);//scene.icamera,{}
	luaL_register(L,NULL,imesh_m);//scene.icamera,{}
	luaL_register(L,NULL,igeneric_m);
	lua_setfield(L,-2,"__index");//scene.icamera
	lua_pop(L,1);//
	
	luaL_newmetatable(L,"scene.icube");//scene.icube
	lua_newtable(L);//scene.icube, {}
	luaL_register(L,NULL,imesh_m);//scene.icube,{}
	luaL_register(L,NULL,icube_m);//scene.icube,{}
	lua_setfield(L,-2,"__index");//scene.icube
	lua_pop(L,1);//

	lua_getglobal(L,"scene");
	lua_pushcfunction(L,newiscenemesh);
	lua_setfield(L,-2,"newmesh");
	lua_pushcfunction(L,newiscenecube);
	lua_setfield(L,-2,"newcube");

}
