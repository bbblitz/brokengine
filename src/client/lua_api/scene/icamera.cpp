
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <memory>
#include <map>
#include <functional>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../gameparts.hpp"
#include "icamera.hpp"
#include "igeneric.hpp"
#include <shared/lua_api/common.hpp>

using namespace irr;
using namespace scene;
using namespace core;

extern IrrlichtDevice* device;

static int newiscenemayacamera(lua_State* L){
	printf("createing maya camera!\n");
	ISceneManager* smgr = device->getSceneManager();
	ICameraSceneNode* cam = smgr->addCameraSceneNodeMaya();
	printf("cam is %p",cam);
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,cam);
	lua_setfield(L,-2,"node");

	//Set it's metatable
	luaL_getmetatable(L, "scene.iscenemayacamera");
	lua_setmetatable(L, -2);

	return 1;
}

// ifpscamera.new() 
static int newiscenefpscamera(lua_State* L){//
	ISceneManager* smgr = device->getSceneManager();
	ICameraSceneNode* cam = smgr->addCameraSceneNodeFPS();
	//cam->bindTargetAndRotation(false);
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,cam);//{},ud_cam
	lua_setfield(L,-2,"node");
	//LISceneNode* lcam = (LISceneNode*)lua_newuserdata(L, sizeof(LISceneNode));//userdata_scenenode
  
	//Set it's metatable
	luaL_getmetatable(L, "scene.ifpscamera");//userdata_scenenode,scene.ifpscamera
	lua_setmetatable(L, -2);//userdata_scenenode
  
	//Create the struct
	//lcam->n = cam;
	//lcam->funcmap = hashmap_new();
	//lcam->type = "iscenefpscamera";
  
	return 1;
}

//iscenecamera.new(Vector position, Vector lookat,{node=parent})
static int newiscenecamera(lua_State* L){
	printf("Createing camera!\n");
	int nargs = lua_gettop(L);
	double px,py,pz;
	double lx,ly,lz;
	ISceneNode* parent = NULL;
	if(nargs == 3){
		lua_getfield(L,-1,"node");//{position},{lookat},{parent},ud_ISceneNode
		parent = (ISceneNode*)lua_touserdata(L,-1);
		lua_pop(L,2);//{position},{lookat}
	}
	//{position},{lookat}	
	//The position of the camera
	popvector3d(L,&px,&py,&pz);//{position}
	printf("position of camera was %f,%f,%f\n",px,py,pz);
	popvector3d(L,&lx,&ly,&lz);//
	printf("lookat of camera was %f,%f,%f\n",lx,ly,lz);
	
	//If the element has a parrent
	if(nargs >= 3){
		printf("got the parrent, %p\n",parent);
	}


	//Create the camera
	ISceneManager* smgr = device->getSceneManager();
	ICameraSceneNode* cam = smgr->addCameraSceneNode(0, vector3df(px,py,pz), vector3df(lx,ly,lz));
	printf("Registered the camera!\n");
	lua_newtable(L);//{}
	lua_pushlightuserdata(L,cam);//{},ud_cam
	lua_setfield(L,-2,"node");//{node=ud_cam}
	luaL_getmetatable(L,"scene.icamera");//{node=ud_cam},{scene.icamera}
	lua_setmetatable(L,-2);//{cam}

	return 1;
}

//camera:bind_target(bool) :: nil
static int icamerabindtarget(lua_State *L){
	int should_bind = lua_toboolean(L,-1);//{node=ud_cam},bool_shouldbind
	printf("Bind target called %d\n",should_bind);
	lua_pop(L,1);//{node=ud_cam}
	lua_getfield(L,-1,"node");//{node=ud_cam},ud_cam
	ICameraSceneNode *cam = (ICameraSceneNode*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	cam->bindTargetAndRotation(should_bind == 1);
	return 0;
}

//camera:gettarget() :: v3f
static int icameragettarget(lua_State *L){
	lua_getfield(L,-1,"node");
	ICameraSceneNode *cam = (ICameraSceneNode*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	vector3df targ = cam->getTarget();
	pushvector3d(L,targ.X, targ.Y, targ.Z);
	return 1;
}

//camera:settarget(v3f)
static int icamerasettarget(lua_State *L){
	double x,y,z;
	popvector3d(L,&x,&y,&z);
	lua_getfield(L,-1,"node");
	ICameraSceneNode *cam = (ICameraSceneNode*)lua_touserdata(L,-1);
	lua_pop(L,2);//
	cam->setTarget(vector3df(x,y,z));
	return 0;
}

static const luaL_reg icamera_m[] = {
	{"bindtarget",      icamerabindtarget},
	{"gettarget",       icameragettarget},
	{"settarget",       icamerasettarget},
	{0,0},
};

static const luaL_reg imayacamera_m[] = {
	{0,0},
};

static const luaL_reg ifpscamera_m[] = {
	{0,0},
};

void icamera_register(lua_State* L){

	luaL_newmetatable(L, "scene.icamera");//scene.icamera
	lua_newtable(L);//scene.icamera, {}
	luaL_register(L,NULL,icamera_m);//scene.icamera, {}
	luaL_register(L,NULL,igeneric_m);//scene.icamera, {}
	lua_setfield(L,-2,"__index");//scene.icamera
	lua_pop(L,1);//

	luaL_newmetatable(L, "scene.imayacamera");//scene.imayacamera
	lua_newtable(L);//scene.imayascamera,{}
	luaL_register(L,NULL,imayacamera_m);//scene.imayascamera,{}
	luaL_register(L,NULL,icamera_m);//scene.imayascamera,{}
	luaL_register(L,NULL,igeneric_m);
	lua_setfield(L,-2,"__index");//scene.imayascamera
	lua_pop(L,1);//

	luaL_newmetatable(L,"scene.ifpscamera");//scene.ifpscamera
	lua_newtable(L);//scene.ifpscamera, {}
	luaL_register(L,NULL,ifpscamera_m);//scene.ifpscamera,{}
	luaL_register(L,NULL,icamera_m);//scene.ifpscamera,{}
	luaL_register(L,NULL,igeneric_m);
	lua_setfield(L,-2,"__index");//scene.ifpscamera
	lua_pop(L,1);//

	lua_getglobal(L,"scene");//{}
	lua_pushcfunction(L,newiscenecamera);//{},newiscenecamera()
	lua_setfield(L,-2,"newcamera");//{}
	lua_pushcfunction(L,newiscenefpscamera);//{},newiscenefpscamera()
	lua_setfield(L,-2,"newfpscamera");//{}
	lua_pushcfunction(L,newiscenemayacamera);//{},newiscenemayacamera()
	lua_setfield(L,-2,"newmayacamera");//{}
	lua_pop(L,1);//
}
