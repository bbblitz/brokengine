#ifndef __H_loadgui
#define __H_loadgui
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

void load_guifuncs(lua_State* L);
#endif
