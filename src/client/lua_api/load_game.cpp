#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

#include <shared/lua_api/common.hpp>
#include "gui/iguibutton.hpp"
#include "gui/iguilabel.hpp"
#include "../callbackhandeler.hpp"
#include "guiparts.hpp"

using namespace irr;
using namespace gui;
using namespace core;

extern video::SColor background;
extern IrrlichtDevice* device;

//exit()
int exit_game(lua_State *L){
	device->closeDevice();
	return 0;
}
int setbackgroundcolor(lua_State* L){
	long r,g,b;
	popvector3i(L,&r,&g,&b);
	background = video::SColor(255,r,g,b);
	return 0;
}

void load_gamefuncs(lua_State* L){
	lua_newtable(L);
	lua_setglobal(L,"GAME");

	lua_getglobal(L,"GAME");
	lua_pushcfunction(L,exit_game);
	lua_setfield(L,-2,"exit");

	lua_pushcfunction(L,setbackgroundcolor);//{game},setbackgroundcolor()
	lua_setfield(L,-2,"setbackgroundcolor");//{game}
	lua_pop(L,1);

	lua_pop(L,1);
}
