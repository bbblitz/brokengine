#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <assert.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>
#include "../callbackhandeler.hpp"
#include "phys/cbphysbox.hpp"
#include "phys/cbphysmodel.hpp"
#include "phys/cbcharactercontroller.hpp"
#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
//#include <shared/phys/physcommon.hpp>
#include <shared/lua_api/common.hpp>
#include <shared/lua_api/load_phys.hpp>
#include <shared/lua_api/phys/bghostobject.hpp>

using namespace irr;
using namespace gui;
using namespace core;

extern IrrlichtDevice* device;
extern btDiscreteDynamicsWorld* World;
extern btBroadphaseInterface *BroadPhase;

//raytest({from},{to},{mins},{maxes}) :: hashit
int raytest(lua_State *L){
	double fx,fy,fz;
	double tx,ty,tz;
	popvector3d(L, &tx, &ty, &tz);
	popvector3d(L, &fx, &fy, &fz);

	btVector3 from(fx, fy, fz);
	btVector3 to(tx, ty, tz);
	btCollisionWorld::RayResultCallback *res = new btDiscreteDynamicsWorld::ClosestRayResultCallback(from, to);
	World->rayTest(from,to,*res);

	lua_pushboolean(L,res->hasHit());
	return 1;
}

void load_cphysfuncs(lua_State* L){
	assert(lua_gettop(L) == 0);
	printf("Registering cphysbox\n");
	//phys things	
	assert(lua_gettop(L) == 0);
	cbphysbox_register(L);
	assert(lua_gettop(L) == 0);
	cbcharactercontroller_register(L);
	assert(lua_gettop(L) == 0);
	cbphysmodel_register(L);
	assert(lua_gettop(L) == 0);
	bghostobject_register(L);
	assert(lua_gettop(L) == 0);

	assert(lua_gettop(L) == 0);
	lua_getglobal(L,"phys");//{}
	lua_pushcfunction(L,raytest);//{},raytest()
	lua_setfield(L,-2,"raytest");//{}
	lua_pop(L,1);
	assert(lua_gettop(L) == 0);
}
