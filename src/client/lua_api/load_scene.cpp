#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
extern "C" {
	#include <lua.h>
	#include <lauxlib.h>
	#include <lualib.h>
}
#include <irrlicht.h>
#include "scene/icamera.hpp"
#include "scene/imesh.hpp"
#include "scene/ilight.hpp"

using namespace irr;

extern IrrlichtDevice* device;

void load_scenefuncs(lua_State* L){
	lua_newtable(L);//{}
	lua_setglobal(L,"scene");//

	//scene things
	icamera_register(L);
	imesh_register(L);
	ilight_register(L);

	//lua_pop(L, 1);
	
}
