/*
    This files runes a lua file that contains settings information, and creates an irrlicht device
    with those settings.
*/
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

#include "initdevice.hpp"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

struct settings{
  u8 aa;
  u8 bits;
  E_DEVICE_TYPE dtype;
  u32 adapter;
  bool doublebuffer;
  bool multithread;
  E_DRIVER_TYPE driver;
  bool fullscreen;
  bool stencilbuffer;
  bool stereobuffer;
  bool performancetimers;
  bool vsync;
  dimension2d<u32> windowsize;
  u8 zbuffer;
};

void parseSetting(const char* settingname, lua_State* L, settings* set){
  if(strcmp(settingname,"Anti Alias") == 0){
    if(lua_isnumber(L,-1)){
      set->aa = lua_tonumber(L,-1);
    }else{
      printf("Setting \"Anti Alias\" must be a number.\n");
    }
  }else if(strcmp(settingname,"Bits Per Pixel") == 0){
    if(lua_isnumber(L,-1)){
      set->bits = lua_tonumber(L,-1);
    }else{
      printf("Setting \"Bits Per Pixel\" must be a number.\n");
    }
  }else if(strcmp(settingname,"Device Type") == 0){
    if(lua_isstring(L,-1)){
      const char* dtn = lua_tostring(L,-1);
      if(strcmp(dtn,"WIN32") == 0){
        set->dtype = irr::EIDT_WIN32;
      }else if(strcmp(dtn,"WINCE") == 0){
        set->dtype = irr::EIDT_WINCE;
      }else if(strcmp(dtn,"OSX") == 0){
        set->dtype = irr::EIDT_OSX;
      }else if(strcmp(dtn,"X11") == 0){
        set->dtype = irr::EIDT_X11;
      }else if(strcmp(dtn,"SDL") == 0){
        set->dtype = irr::EIDT_SDL;
      }else if(strcmp(dtn,"CONSOLE") == 0){
        set->dtype = irr::EIDT_CONSOLE;
      }else if(strcmp(dtn,"BEST") == 0){
        set->dtype = irr::EIDT_BEST;
      }
    }else{
      printf("Setting \"Device Type\" must be a string\n");
    }
  }else if(strcmp(settingname,"Display Adapter") == 0){
    if(lua_isnumber(L,-1)){
      set->adapter = lua_tonumber(L,-1);
    }else{
      printf("Setting \"Display Adapter\" must be a number\n");
    }
  }else if(strcmp(settingname,"Double Buffer") == 0){
    if(lua_isboolean(L,-1)){
      set->doublebuffer = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"Double Buffer\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"Multithreaded") == 0){
    if(lua_isboolean(L,-1)){
      set->multithread = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"Multithreaded\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"Driver Type") == 0){
    if(lua_isstring(L,-1)){
      const char* dts = lua_tostring(L,-1);
      if(strcmp(dts,"NULL") == 0){
        set->driver = EDT_NULL;
      }else if(strcmp(dts,"SOFTWARE") == 0){
        set->driver = EDT_SOFTWARE;
      }else if(strcmp(dts,"BURNINGS") == 0){
        set->driver = EDT_BURNINGSVIDEO;
      }else if(strcmp(dts,"D3D9") == 0){
        set->driver = EDT_DIRECT3D9;
      }else if(strcmp(dts,"OPENGL") == 0){
        set->driver = EDT_OPENGL;
      }
    }else{
      printf("Setting \"Driver Type\" must be a string\n");
    }
  }else if(strcmp(settingname,"Fullscreen") == 0){
    if(lua_isboolean(L,-1)){
      set->fullscreen = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"Fullscreen\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"Stencil Buffer") == 0){
    if(lua_isboolean(L,-1)){
      set->stencilbuffer = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"Stencil Buffer\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"Stereo Buffer") == 0){
    if(lua_isboolean(L,-1)){
      set->stereobuffer = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"Stereo Buffer\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"VSync") == 0){
    if(lua_isboolean(L,-1)){
      set->vsync = lua_toboolean(L,-1) == 1;
    }else{
      printf("Setting \"VSync\" must be a boolean\n");
    }
  }else if(strcmp(settingname,"Window Width") == 0){
    if(lua_isnumber(L,-1)){
      set->windowsize.Width = lua_tonumber(L,-1);
    }else{
      printf("Setting \"Window Width\" must be a number");
    }
  }else if(strcmp(settingname,"Window Height") == 0){
    if(lua_isnumber(L,-1)){
      set->windowsize.Height = lua_tonumber(L,-1);
    }else{
      printf("Setting \"Window Height\" must be a number");
    }
  }
}

void settingsFromTable(lua_State *L, SIrrlichtCreationParameters* p){
  lua_pushnil(L);//nil
  settings* set = (settings*)malloc(sizeof(settings));
  printf("Loading settings...");
  while(lua_next(L,-2) != 0){
    if(lua_isstring(L,-2)){
      const char* setstr = lua_tostring(L,-2);
      parseSetting(setstr,L,set);
    }else{
      printf("\tFound a non-string setting key! Key is a %s\n",luaL_typename(L,-1));
    }
    lua_pop(L,1);
  }
  p->AntiAlias = set->aa;
  p->Bits = set->bits;
  p->DeviceType = set->dtype;
  p->DisplayAdapter = set->adapter;
  p->DriverMultithreaded = set->multithread;
  p->DriverType = set->driver;
  p->Fullscreen = set->fullscreen;
  p->Stencilbuffer = set->stencilbuffer;
  p->Vsync = set->vsync;
  p->WindowSize = set->windowsize;
  p->ZBufferBits = set->zbuffer;
  free(set);
  printf("[OK]\n");
}

IrrlichtDevice* spawnIrrDevice(lua_State* L, char *path){
	//printf("Attempting to load settings...\n");
	char initname[] = "deviceinit.lua";
	size_t pathlen = strlen(initname) + strlen(path);
	char filename[pathlen + 1];
	sprintf(filename,"%s/%s",path,initname);
	int iErr = luaL_dofile(L,filename);//ret
	SIrrlichtCreationParameters p = SIrrlichtCreationParameters();
	if(iErr != 0){
		printf("Failed to open lua file:%s\n", filename);
	}
	settingsFromTable(L,&p);
	IrrlichtDevice* dev = createDeviceEx(p);
	if(!dev)
		exit(1);
	dev->setWindowCaption(L"Brokengine");
	lua_pop(L,1);
	return dev;
}
