#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

#include <chrono>

#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
#include <cstdlib>

#include "initdevice.hpp"
#include "menuhandeler.hpp"
#include "lua_api/load_gui.hpp"
#include "lua_api/load_game.hpp"
#include "lua_api/load_scene.hpp"
#include "lua_api/load_cphys.hpp"
#include "lua_api/load_video.hpp"
#include "lua_api/load_io.hpp"
#include <shared/lua_api/load_phys.hpp>
#include "callbackhandeler.hpp"

#include <shared/lua_api/common.hpp>
#include "../shared/lua_api/load_net.hpp"
#include "../shared/phys/physcommon.hpp"
#include "../shared/lua_api/load_common.hpp"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std::chrono;

lua_State* L;
IrrlichtDevice* device;
IVideoDriver* driver;
void loadIrrLibs(lua_State* L, IrrlichtDevice* device){
	printf("Loading guifuncs...");
	load_guifuncs(L);
	printf("[OK]\n");
	printf("Loading gamefuncs...");
	load_gamefuncs(L);
	printf("[OK]\n");
	printf("Loading scenefuncs...");
	load_scenefuncs(L);
	printf("[OK]\n");
	printf("Loading physfuncs...");
	load_physfuncs(L);
	load_cphysfuncs(L);
	printf("[OK]\n");
	printf("Loading videofuncs...");
	load_videofuncs(L);
	printf("[OK]\n");
	load_iofuncs(L);
}

void dropCollisionObject(btCollisionObject* rb){
	ISceneNode *node = static_cast<ISceneNode*>(rb->getUserPointer());
	if(node)
		node->remove();
}

void dropChar(btKinematicCharacterController *a){
	ISceneNode *node = (ISceneNode*)a->getGhostObject()->getUserPointer();
	if(node)
		node->remove();
}

void dropGhostObject(btGhostObject *ghost){
	ISceneNode *node = (ISceneNode*)ghost->getUserPointer();
	if(node)
		node->remove();
}

// Converts a quaternion to an euler angle
void QuaternionToEuler(const btQuaternion &TQuat, btVector3 &TEuler) {
	btScalar W = TQuat.getW();
	btScalar X = TQuat.getX();
	btScalar Y = TQuat.getY();
	btScalar Z = TQuat.getZ();
	float WSquared = W * W;
	float XSquared = X * X;
	float YSquared = Y * Y;
	float ZSquared = Z * Z;

	TEuler.setX(atan2f(2.0f * (Y * Z + X * W), -XSquared - YSquared + ZSquared + WSquared));
	TEuler.setY(asinf(-2.0f * (X * Z - Y * W)));
	TEuler.setZ(atan2f(2.0f * (X * Y + Z * W), XSquared - YSquared - ZSquared + WSquared));
	TEuler *= core::RADTODEG;
}

void UpdateElement(btCollisionObject* obj){

	if(obj->getUserPointer() != NULL){
		//UpdateRender(*Iterator);
		scene::ISceneNode *node = static_cast<scene::ISceneNode *>(obj->getUserPointer());

		// Set position
		btTransform transform = obj->getWorldTransform();
		btVector3 pos = transform.getOrigin();
		node->setPosition(core::vector3df((f32)pos[0], (f32)pos[1], (f32)pos[2]));
		//printf("Setting new position to %f %f %f\n", pos[0], pos[1], pos[2]);

		//Set rotation
		btQuaternion rot = transform.getRotation();
		btVector3 eulerrot;
		QuaternionToEuler(rot,eulerrot);
		node->setRotation(core::vector3df(eulerrot[0], eulerrot[1], eulerrot[2]));
	}

}

SColor background = SColor(255,100,101,140);

//setbackgroundcolor(r,g,b)

int main(int argc, char *argv[]){
	printf("Brok[en]gine Client\n");

	// Initialize bullet
	phys_genesis();

	//Create a new lua state, this gets shared everywhere
	//Set the path for lua
	putenv("LUA_PATH=?.lua");
	putenv("LUA_CPATH=../bin/?.dll");
	lua_State *state = luaL_newstate();
	L = state;
	printf("Created lua state at %p\n",L);
	//Load the lua libraries
	assert(lua_gettop(L) == 0);
	loadLLibs(state);
	assert(lua_gettop(L) == 0);
	//Defined in initdevice.cpp, creates the irrlicht device
	printf("Argc: %d\n",argc);
	char *path;
	if(argc == 2){
		path = argv[1];
	}else{
		path = (char*)"../data";
	}
	assert(lua_gettop(L) == 0);
	device = spawnIrrDevice(state,path);
	assert(lua_gettop(L) == 0);
	if (!device){
		printf("Failed to initalize device\n");
		return 1;
	}
	device->getFileSystem()->changeWorkingDirectoryTo(path);
	//ILogger* log = device->getLogger();
	//log->setLogLevel(ELL_NONE);
	//Loads libraries for interfaceing with irrlicht
	assert(lua_gettop(L) == 0);
	luaL_openlibs(state);
	assert(lua_gettop(L) == 0);
	loadIrrLibs(state,device);
	assert(lua_gettop(L) == 0);
	loadNetLibs(state);
	assert(lua_gettop(L) == 0);
	loadCommonLibs(state);
	assert(lua_gettop(L) == 0);
	printf("Loadded irr libs...\n");
	//Sets the global event handeler
	printf("Creating event receiver\n");
	GlobalEventReceiver ger = GlobalEventReceiver(device);
	printf("Created event receiver\n");
	device->setEventReceiver(&ger);
	assert(lua_gettop(L) == 0);
	pusherrorfunc(L);//errfunc
	int err = luaL_loadfile(state,"init.lua");//errfunc,init()
	printf("Error loading init.lua: %d\n",err);
	switch(err){//errmsg or nothing
		case 0:
			break; //no error
		case LUA_ERRSYNTAX:
			printf("Syntax error, failed to load: %s\n%s\n","../data/init.lua",lua_tostring(L,-1));
			break;
		case LUA_ERRMEM:
			printf("Failed to allocate memroy\n");
			break;
		case LUA_ERRFILE:
			printf("Could not find file: %s\n","../data/init.lua");
			break;
	}
	//errfunc,initfile()
	printf("Loaded file\n");
	lua_pcall(state,0,0,-2);//errfunc()
	lua_pop(L,1);//
	printf("Finished running init.lua");
	assert(lua_gettop(L) == 0);
	//int iErr = luaL_dofile(state,"init.lua");
	//if(iErr != 0){
		//printf("Failed to open lua file:%s/init.lua\n",path);
		//lua_error(state);
	//}

	//Load some bullet physics stuff
	driver = device->getVideoDriver();
	ISceneManager* smgr = device->getSceneManager();
	IGUIEnvironment* guienv = device->getGUIEnvironment();

	device->setWindowCaption(L"Brok[en]gine Client v0.1\n");

	printf("Everything registered, about to start running device!\n");

	lua_getglobal(state,"GAME");//{game}
	lua_pop(state,1);//

	printf("About to check if device run\n");
	printf("Device is %p\n",device);
	while(device->run()){
		//printf("Start gameloop net\n");
		assert(lua_gettop(L) == 0);
		gameloop_net(L);
		assert(lua_gettop(L) == 0);
		//printf("End gameloop net\n");
		//printf("Start gameloop phys\n");
		assert(lua_gettop(L) == 0);
		gameloop_phys(UpdateElement);
		assert(lua_gettop(L) == 0);
		//printf("End gameloop phys\n");
		if(device->isWindowActive()){
			assert(lua_gettop(L) == 0);
			driver->beginScene(true, true, background);
			assert(lua_gettop(L) == 0);
			//printf("Device active, began scene\n");
			assert(lua_gettop(L) == 0);
			pusherrorfunc(state);
			lua_getglobal(state,"GAME");//err(),{GAME}
			lua_getfield(state,-1,"draw");//err(),{GAME},GAME.draw()
			if(!lua_isnil(state,-1)){
				lua_pcall(state,0,0,-3);
				lua_pop(state,2);
			}else{
				lua_pop(state,3);
			}
			assert(lua_gettop(L) == 0);
			//printf("Finished calling GAME.draw()\n");

			smgr->drawAll();
			//printf("Scene manager drew all\n");
			assert(lua_gettop(L) == 0);
			lua_getglobal(state,"GAME");
			lua_getfield(state,-1,"drawPostScene");
			if(!lua_isnil(state,-1)){
				lua_call(state,0,0);
				lua_pop(state,1);
			}else{
				lua_pop(state,2);
			}
			assert(lua_gettop(L) == 0);
			//printf("Post draw scene completed\n");
			guienv->drawAll();
			//printf("Gui draw all completed\n");
			assert(lua_gettop(L) == 0);
			lua_getglobal(state,"GAME");
			lua_getfield(state,-1,"drawPostGui");
			if(!lua_isnil(state,-1)){
				lua_call(state,0,0);
				lua_pop(state,1);
			}else{
				lua_pop(state,2);
			}
			assert(lua_gettop(L) == 0);
			//printf("GAME.drawPostGui completed\n");
			driver->endScene();
		}else{
			assert(lua_gettop(L) == 0);
			device->yield();
			assert(lua_gettop(L) == 0);
		}
		assert(lua_gettop(L) == 0);
		pusherrorfunc(L);//errfunc()
		lua_getglobal(state,"GAME");//errfunc(),{}
		lua_getfield(state,-1,"tick");//errfunc(),{},function_tick()
		if(!lua_isnil(state,-1)){
			lua_pcall(state,0,0,-3);//errfunc(),{}
			lua_pop(state,2);//
		}else{//errfunc(),{},nil
			lua_pop(state,3);//
		}
		assert(lua_gettop(L) == 0);
	}
	assert(lua_gettop(L) == 0);
	//phys_shutdown(RemoveISceneNode);
	phys_shutdown();
	device->drop();
	printf("Goodbye\n");
	return 0;
}
