#ifndef __H_BE_initdevice
#define __H_BE_initdevice
#include <irrlicht.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

IrrlichtDevice* spawnIrrDevice(lua_State* L, char *path);
#endif
