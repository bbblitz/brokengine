#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <irrlicht.h>

using namespace irr;
using namespace gui;

//void registerguicallback(IGUIElement* element, EGUI_EVENT_TYPE event, bool (*func)(irr::SEvent));

void registerguielement(lua_State* L, gui::EGUI_EVENT_TYPE et, const char* funcname);

class GlobalEventReceiver : public irr::IEventReceiver{
public:
  GlobalEventReceiver(IrrlichtDevice* d);
  bool OnEvent(const irr::SEvent& e);
};

int errfunc(lua_State *L);
void pusherrorfunc(lua_State *L);
//GlobalEventReceiver::GlobalEventReceiver(IrrlichtDevice* d);
