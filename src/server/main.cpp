
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

//C++ things
#include <vector>
#include <thread>
#include <list>

#include <string.h> // for strstr()

#include <btBulletDynamicsCommon.h>
#include <cstdlib>

#include <shared/lua_api/common.hpp>
#include <shared/lua_api/load_net.hpp>
#include <shared/lua_api/load_phys.hpp>
#include <shared/phys/physcommon.hpp>
#include <shared/util/hashmap.hpp>

#include <server/lua_api/load_game.hpp>
#include <server/lua_api/load_io.hpp>

using namespace std;
using namespace chrono;

bool game_active = true;

void dropRigidBody(btRigidBody* rb){
}

void dropCollisionObject(btCollisionObject* obj){
}

void dropChar(btKinematicCharacterController *a){
}

void dropGhostObject(btGhostObject *ghost){
}

lua_State* L;
void gameloop(){
	gameloop_phys(NULL);
	//printf("done physics\n");
	gameloop_net(L);
	//printf("done net\n");
}

int main (int argc, char *argv[]){
	printf("Brok[en]gine Server\n");
	game_active = true;
	char *path;
	if(argc == 2){
		path = argv[1];
	}else{
		path = (char*)"../data";
	}
	size_t envstrsize = snprintf(NULL,0,"LUA_PATH=%s/?.lua",path);
	char envstr[envstrsize];
	sprintf(envstr,"LUA_PATH=%s/?.lua",path);
	putenv(envstr);
	//printf("Put lua path\n");
	L = luaL_newstate();
	assert(lua_gettop(L) == 0);
	//printf("Created lua state\n");
	//lua_newtable(L);//{}
	//lua_setglobal(L,"GAME");//
	assert(lua_gettop(L) == 0);
	load_gamefuncs(L);
	assert(lua_gettop(L) == 0);
	printf("Created global table\n");

	assert(lua_gettop(L) == 0);
	phys_genesis();
	printf("Started phys\n");
	assert(lua_gettop(L) == 0);
	luaL_openlibs(L);
	printf("Opened standard libs\n");
	assert(lua_gettop(L) == 0);
	loadLLibs(L);
	printf("Opened aux libs\n");
	assert(lua_gettop(L) == 0);
	loadNetLibs(L);
	printf("Opened net libs\n");
	assert(lua_gettop(L) == 0);
	load_physfuncs(L);
	printf("Opened phys libs\n");
	assert(lua_gettop(L) == 0);
	load_iofuncs(L);
	printf("About to push error func\n");
	assert(lua_gettop(L) == 0);
	pusherrorfunc(L);//errfunc
	printf("pushed error func\n");
	size_t init_file_path_len = snprintf(NULL,0,"%s/init.lua",path);
	char init_file_path[init_file_path_len];
	sprintf(init_file_path,"%s/init.lua",path);
	switch(luaL_loadfile(L,init_file_path)){
		case 0:
			break; //no error
		case LUA_ERRSYNTAX:
			printf("Syntax error, failed to load: %s\n%s",init_file_path,lua_tostring(L,-1));
			break;
		case LUA_ERRMEM:
			printf("Failed to allocate memroy\n");
			break;
		case LUA_ERRFILE:
			printf("Could not find file: %s\n",init_file_path);
			break;
	}
	//errfunc,initfile()
	printf("Loaded file\n");
	lua_pcall(L,0,0,-2);//errfunc
	lua_pop(L,1);
	assert(lua_gettop(L) == 0);
	do{
		assert(lua_gettop(L) == 0);
		//printf("Start of server gameloop\n");
		gameloop();
		assert(lua_gettop(L) == 0);
		//printf("Gameloop\n");
		//std::this_thread::yield();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		//printf("Thread yeild\n");
		assert(lua_gettop(L) == 0);
		pusherrorfunc(L);//errfunc()
		lua_getglobal(L,"GAME");//errfunc(),{}
		lua_getfield(L,-1,"tick");//errfunc(),{},function_tick()?
		if(!lua_isnil(L,-1)){
			//printf("Found game tick\n");
			lua_pcall(L,0,0,-3);//errfunc(),{}
			lua_pop(L,2);
		}else{
			//printf("Did not find tick function\n");
			lua_pop(L,3);
		}
		assert(lua_gettop(L) == 0);
		//printf("End of server gameloop\n");
	}while(game_active);
	assert(lua_gettop(L) == 0);
	phys_shutdown();
	assert(lua_gettop(L) == 0);

	printf("Goodbye\n");
	return 0;
}
