// 
// // Minimal C ZMQ Client example
// 
// // Properties -> Linker -> Input -> Additional dependencies "libzmq.lib"
// // Properties -> Linker -> Input -> Additional dependencies "ws2_32.lib"
// 
// #include <stdio.h>
// #include "zmq.h"
// 
// 
// #define WS_VER 0x0202
// #define DEFAULT_PORT "5001"
// 
// 
// int main ()
// {
//   int retval;
//   void *ctx, *s;
//   zmq_msg_t query, reply;
//   const char *query_string = "SELECT * FROM sometable, END SERVER", *reply_string;
// 
// 
// printf( "Initialise ZMQ context: \n" );
// 
//     // Initialise ZMQ context, requesting a single application thread and a single I/O thread 
//     ctx = zmq_init(1);
//     if( ctx == 0 ) { 
// 			printf( "Error zmq_init: '%s'\n",  zmq_strerror(errno) );
// 			return 0; 
// 		}
// 
//     while(1) {
// printf( "Create a ZMQ_REP socket: \n" );
// 				// Create a ZMQ_REP socket to receive requests and send replies 
// 				s = zmq_socket( ctx, ZMQ_REQ );
// 				if( s == 0 ) {
// 					printf( "Error zmq_socket: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				} 
// 
// 				retval = zmq_connect( s, "tcp://localhost:5555" );
// 				if( retval != 0 ) {
// 					printf( "Error zmq_connect: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				} 
// 
// 
// 				// Allocate a message and fill it 
// 				retval = zmq_msg_init_size( &query, strlen(query_string)+1 );
// 				if( retval != 0 ) { 
// 					printf( "Error zmq_msg_init_size: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				}
// 
// 				memcpy( zmq_msg_data (&query), query_string, strlen (query_string)+1 );
//  
// printf( "Send Query: '%s'\n", query_string );
// 				// Send Query 
// 				retval = zmq_send( s, &query, 0 , 0);
// 				if( retval != 0 ) {
// 					printf( "Error zmq_send: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				}
// 
// 
// 
// 				// Recover response
// 				zmq_msg_init(&reply);
// 				if( retval != 0 ) { 
// 					printf( "Error zmq_msg_init: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				}
// 
// printf( "Waiting for reply: \n");
// 
// 				// Receive reply, blocks until one is available 
// 				retval = zmq_recv( s, &reply, 0 , 0);
// 				if( retval != 0 ) { 
// 					printf( "Error zmq_recv: '%s'\n", zmq_strerror(errno) );
// 					break; 
// 				}
//  
// 				// Process the reply 
// 				reply_string = (const char *)zmq_msg_data(&reply);
// printf("Server reply: '%s'\n", reply_string);
// 
// 
// 				// Free message memory
// 				zmq_msg_close(&reply);
// 
// 				break;
// 		} 
// 
// 
// 		// Close connection 
// 		if( s != 0) { 
// printf( "Close connection \n");
// 			retval = zmq_close(s);
// 			if( retval != 0 ) { 
// 				printf( "Error zmq_close: '%s'\n", zmq_strerror(errno) );
// 			}
// 		}
// 
// 		// Release library resources 
// 		retval = zmq_term(ctx);
// 
// printf( "All Done \n" );
// 
//   return 0;
// }
