#ifndef __H_loadgame
#define __H_loadgame
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

void load_gamefuncs(lua_State* L);
#endif
