#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

extern bool game_active;

//exit()
int exit_game(lua_State *L){
	game_active = false;
	return 0;
}

void load_gamefuncs(lua_State* L){
	lua_newtable(L);
	lua_setglobal(L,"GAME");

	lua_getglobal(L,"GAME");
	lua_pushcfunction(L,exit_game);
	lua_setfield(L,-2,"exit");
	lua_pop(L,1);
}
