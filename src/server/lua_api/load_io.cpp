
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}
#include <server/lua_api/load_io.hpp>
//STL
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#define epath "../data"
#define epathlen sizeof(epath)

/*Add an io.dir(path) function, which lists all the files in (path)*/
int dirpath(lua_State *L){
	if(!lua_isstring(L,-1)){
		lua_pushstring(L,"io.dir() requires a string as argument #1");
		lua_error(L);
	}
	size_t pathstrlen;
	const char *pathstr = lua_tolstring(L,-1,&pathstrlen);
	//printf("got pathstr: %s\n",pathstr);
	//char tpathstr[pathstrlen + epathlen + 1 + 1]; //+1 for null, +1 for /
	//memcpy(tpathstr,epath,epathlen);
	//tpathstr[epathlen] = '/';
	//memcpy(tpathstr+epathlen,pathstr,pathstrlen);
	//tpathstr[pathstrlen + epathlen + 1] = '\0';
	//printf("tpathstr is: \"%s\"\n",tpathstr);
	//lua_pop(L,1);
	DIR *dir;
	struct dirent *ent;
	const char *tpathstr = pathstr;
	dir = opendir(tpathstr);
	if(dir == NULL){
		perror("Cannot open");
		lua_pushstring(L,"Failed to open directory: ");
		lua_pushstring(L,tpathstr);
		lua_concat(L,2);
		lua_error(L);
	}
	int i = 1;
	ent = readdir(dir);
	lua_newtable(L);
	while( (ent = readdir(dir))  != NULL){
		lua_pushinteger(L,i);
		lua_pushstring(L,ent->d_name);
		lua_settable(L,-3);
		i++;
	}
	closedir(dir);
	return 1;
}


void load_iofuncs(lua_State* L){
	lua_getglobal(L,"io");

	lua_pushcfunction(L,dirpath);
	lua_setfield(L,-2,"dir");
	lua_pop(L,1);
}
