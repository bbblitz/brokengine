#ifndef __H_loadio
#define __H_loadio
#include <stdio.h>
#include <stdlib.h>
#include <vector>
extern "C" {
  #include <lua.h>
  #include <lauxlib.h>
  #include <lualib.h>
}

void load_iofuncs(lua_State* L);
#endif
