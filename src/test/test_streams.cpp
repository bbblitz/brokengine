#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../shared/lua_api/stream.hpp"

int main(){
	//Simple
	struct stream* s = stream_create();
	stream_writeInt(s,5);
	assert(stream_readInt(s) == 5);
	stream_free(s);

	//Little more complex
	s = stream_create();
	stream_writeInt(s,1);
	stream_writeInt(s,2);
	stream_writeInt(s,3);
	stream_writeInt(s,4);
	assert(stream_readInt(s) == 1);
	assert(stream_readInt(s) == 2);
	assert(stream_readInt(s) == 3);
	assert(stream_readInt(s) == 4);
	stream_free(s);

	//Make sure we're not leaking memory
	//Uncomment this section, and check htop or windows task manager
	/*
	time_t t;
	time(&t);
	srand((unsigned) t);
	int i,j;
	for(i = 0; i < 100000; i++){
		s = stream_create();
		for(j = 0; j < 10000; j++){
			stream_writeInt(s,rand());
		}
		stream_free(s);
	}
	*/

	//Read/write data
	s = stream_create();
	char* str = (char*)"One two three four!\n";
	int slen = strlen(str);
	stream_writeData(s,str,slen);
	char out[slen+1];
	stream_readData(s,slen,out);
	out[slen] = '\0';
	assert(strcmp(out,str) == 0);
	stream_free(s);

	//Read/write string
	s = stream_create();
	stream_writeString(s,str,slen);
	char* rstr = stream_readString(s);
	assert(strcmp(rstr,str) == 0);
	stream_free(s);

	//Make sure the string reads it's null character
	s = stream_create();
	stream_writeString(s,str,slen);
	stream_writeInt(s,5);
	stream_print(s);
	stream_readString(s);
	stream_print(s);
	int i = stream_readInt(s);
	assert(i == 5);
	stream_free(s);

	printf("OK!\n");
	return 0;
}
