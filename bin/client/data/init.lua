print([[
Welcome to Brok[en]gine!

This file is located at bin/client/data/init.lua,
it is the first file run after brokengine starts.
Modify it to make your game.

Before brokengine starts, there's another file
located at bin/client/data/deviceinit.lua, which
is a settings file for the window, and does not
have access to a lot of the functions that
brokengine provides. If your game is crashing
right away, check for errors in that file.

For more information about the functions
avaliable in brokengine, run `ldoc .`, and look
at index.html in the doc/ folder.
]])

