print("device init called")
return {
    --[[
        Anti-Alias
        Should the window use fullscreen anti aliasing
        Default:16
    ]]
    ["Anti Alias"] = 16,
    --[[
        Bits Per Pixel
        The minimum bits per pixel of the color buffer in fullscreen. Ignored in window mode.
        Default:16
    ]]
    ["Bits Per Pixel"] = 16,
    --[[
        Device Type
        Options:
            WIN32 - Only avaliable on windows desktops
            WINCE - Only avaliable on windows mobile
            COCOA - Only avaliable on OSX
            X11   - Avaliable on Linux, Solaris, BSD, anyone that uses X11
            SDL   - Avaliable on most systems
            CONSOLE - Usually avaliable, but can only render text
            BEST  - Automatically choose the best device.
        Default:Best
    ]]
    ["Device Type"] = "BEST",
    --[[
        Display Adapter
        Pick which graphics card is used for rendering when there is more than one.
        Default:0
    ]]
    ["Display Adapter"] = 0,
    --[[
        Double Buffer
        Should the window use doublebuffering?
        Default:false
    ]]
    ["Double Buffer"] = true,
    --[[
        Multithreaded
        Should the display use multiple threads?
        Default:false
    ]]
    ["Multithreaded"] = false,
    --[[
        Driver Type
        The video driver used to render graphics
        Options:
            NULL        - You probably don't want this one
            SOFTWARE    - Donate your computer to a mueseum
            BURNINGS    - a software alternative
            D3D8        - Direct 3D 8 Win32 only
            D3D9        - Direct 3D 9 Win32 only
            OPENGL      - Open GL
        (vulkan support comming soon... maybe.)
        Default:OPENGL
    ]]
    ["Driver Type"] = "OPENGL",
    --[[
        Fullscreen
        Should the window be fullscreen?
        Default:false
    ]]
    ["Fullscreen"] = false,
    --[[
        Stencil buffer
        Should the stencil buffer be enabled?
        Default:false
    ]]
    ["Stencil Buffer"] = true,
    --[[
        Stereo Buffer
        Should the window use stereo buffers?
        Default:false
    ]]
    ["Stereo Buffer"] = false,
    --[[
        Vertical Sync
        Should the frame wait to be displayed to screen before starting the next draw?
        Enable this if you are getting graphical artifacts
        Default:false
    ]]
    ["VSync"] = true,
    --[[
        Window Width/height
        Adjusts the size of the window.
        Default: 640,480
    ]]
    ["Window Width"] = 640,
    ["Window Height"] = 480,
}
