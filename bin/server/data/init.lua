print("Hello from server!")


xpcall(function()
	local socket = net.newsocket(net.REP)
	socket:bind("tcp://127.0.0.1:5555")
	socket:receive(function(stream)
		print("Server got message:", stream:readstring())
	end)
end,function(err)
	print("Error:",err)
	print(debug.traceback())
end)

print("Socket is bound")
