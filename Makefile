
#Detect what kind of system we're on
ifeq ($(OS), Windows_NT)
	UNAME:=Windows
else
	UNAME:=$(shell uname -s)
endif
Q=@
ifeq ($(UNAME), Windows)
include Makefile.win
endif
ifeq ($(UNAME), Linux)
include Makefile.nix
endif
