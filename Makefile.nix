

include Makefile.shared

shared_objs=$(SHARED_SRC:src/shared/%=build/shared/%.o)
client_objs=$(CLIENT_SRC:src/client/%=build/client/%.o)
server_objs=$(SERVER_SRC:src/server/%=build/server/%.o)
client_bins=bin/client/bin/brokengine_client
server_bins=bin/server/bin/brokengine_server
bins=$(client_bins) $(server_bins)

all: $(bins)
	echo "Done"

clean:
	$(RM) $(client_objs)
	$(RM) $(server_objs)
	$(RM) $(shared_objs)
	$(RM) $(bins)

CLIENT_LIB_DIRS=\
	-Llib/luajit/src\
	-Llib/irrlicht/lib/Linux\
	-Llib/bullet/src/BulletCollision\
	-Llib/bullet/src/BulletDynamics\
	-Llib/bullet/src/LinearMath\
	-Llib/nng/static

CLIENT_LIBS=\
	-lluajit\
	-lIrrlicht\
	-lGL\
	-lX11\
	-lXxf86vm\
	-lBulletDynamics\
	-lBulletCollision\
	-lLinearMath\
	-lnng\

bin/client/bin/brokengine_client : $(client_objs) $(shared_objs)
	$(CXX) $(LDFLAGS) -o $@ $^ $(CLIENT_LIB_DIRS) $(CLIENT_LIBS)

bin/server/bin/brokengine_server : $(server_objs) $(shared_objs)
	$(CXX) $(LDFLAGS) -o $@ $^ $(CLIENT_LIB_DIRS) $(CLIENT_LIBS)

$(shared_objs) : build/shared/%.o : src/shared/%.cpp src/shared/%.hpp
	$(CXX) $(CFLAGS) -c -o $@ $<

$(client_objs) : build/client/%.o : src/client/%.cpp src/client/%.hpp
	$(CXX) $(CFLAGS) -c -o $@ $<

$(server_objs) : build/server/%.o : src/server/%.cpp src/server/%.hpp
	$(CXX) $(CFLAGS) -c -o $@ $<
